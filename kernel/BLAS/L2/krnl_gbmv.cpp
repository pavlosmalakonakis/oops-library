#include "../../include/global.hpp"
#include "../../include/common.hpp"
#define block_size 2

//layout is for specific FPGA to check available sizes
extern "C" {
void krnl_gbmv(const char TransA, const int M, const int N, const int KL, const int KU, const float alpha, const float *A, const int lda,
                 const float  *X, const int incX, const float beta, float  *Y, const int incY) {

#pragma HLS INTERFACE m_axi port = X offset = slave bundle = ddr0
#pragma HLS INTERFACE m_axi port = Y offset = slave bundle = ddr0
#pragma HLS INTERFACE m_axi port = A offset = slave bundle = ddr1

#pragma HLS INTERFACE s_axilite port = N
#pragma HLS INTERFACE s_axilite port = M
#pragma HLS INTERFACE s_axilite port = KL
#pragma HLS INTERFACE s_axilite port = KU
#pragma HLS INTERFACE s_axilite port = lda
#pragma HLS INTERFACE s_axilite port = TransA
#pragma HLS INTERFACE s_axilite port = incX
#pragma HLS INTERFACE s_axilite port = incY
#pragma HLS INTERFACE s_axilite port = alpha
#pragma HLS INTERFACE s_axilite port = beta


static hls::stream<float> Ain;
static hls::stream<float> Ytemp;
static hls::stream<float> Yout;
#pragma HLS STREAM variable = Yout depth = 2048
#pragma HLS STREAM variable = Ytemp depth = 2048
#pragma HLS STREAM variable = Ain depth=2048
int row_size=M;
int column_size=N;
if(TransA=='C' or TransA=='T'){
    row_size=N;
    column_size=M;
}
int item,index;
int index_x=0;
int starting_index,ending_index;
float x_b[block_size];

    if(TransA=='C' or TransA=='T'){

    }
    else if (TransA=='N'){
    #pragma HLS dataflow
    	main_loop:
    	for (int j=0;j<N/block_size;j++){
			loop_1:
			index_x=set_x_b(block_size,index_x,X,incX,x_b,alpha);
			loop_2:
			for (int k=0;k<block_size;k++){
				index=j*block_size+k;
				starting_index=max_(0,index-KU);
				ending_index=min_(M,index-KU);
				read_vector_matrix(A,Ain,starting_index+index,ending_index*N+index,N);
				read_vector_matrix(Y, Yout,starting_index, ending_index+1,incY);
				scal(Ain,Ytemp,ending_index-starting_index,x_b[k]);
				add_streams_to_stream_N_items(Yout,Ytemp,Ain,ending_index-starting_index);
				write_multiple_items_from_fifo(Y,Ain,starting_index, ending_index+1,incY);
			}
		}
    }
}}
