#include <hls_stream.h>
#include <ap_int.h>
#include <hls_math.h>
//#include <stdio.h>

#define VDATA_SIZE 16
typedef struct v_datatype { float data[VDATA_SIZE]; } v_dt;

void wide_read_x(hls::stream<v_dt>& Xup1, hls::stream<v_dt>& Xup2, hls::stream<v_dt>& Xlow1, hls::stream<v_dt>& Xlow2, v_dt *X,int currCol){

	//printf("\nRead X\n");
	v_dt Xtemp;
	#pragma HLS pipeline II=1
	Xtemp=X[currCol];
	Xlow1 << Xtemp;
	Xlow2 << Xtemp;
	Xup1 << Xtemp;
	Xup2 << Xtemp;
	//printf("%d",currCol);

}

void read_y(hls::stream<float>& Yupper,float *Y,int M){

//	printf("\nRead Y\n");

	for(int i=0;i<M;i++){
		#pragma HLS pipeline II=1
		Yupper <<Y[i];
//		printf("%d=%f \t",i,Y[i]);
	}
}

void write_y(hls::stream<float>& Yupper,float *Y,int M){

//	printf("\nWrite Y\n");

	for(int i=0;i<M;i++){
		#pragma HLS pipeline II=1
		Y[i] = Yupper.read();
//		printf("%d=%f \t",i,Y[i]);
	}
}

void wide_read_matrix(v_dt *A, hls::stream<v_dt>& AStream, int M, int Ncols, int currCol){

//	printf("\nRead A\n");

	wide_read:
	for(int i=0;i<M;i++){
		#pragma HLS pipeline II=1
		AStream << A[i*Ncols+currCol];
//		printf("%d \t",i*Ncols+currCol);
	}
}

void gemv(hls::stream<v_dt>& Ain, hls::stream<v_dt>& Y,hls::stream<v_dt>& X, float alpha, int Mrows){

//	printf("\nExecute GEMV \n");
	v_dt Xtemp=X.read();
//	printf("X = %f\n",Xtemp.data[0]);
	v_dt Atemp;
	v_dt Ytemp;
	gemv:
	for(int i=0;i<Mrows;i++){
		#pragma HLS pipeline II=1
		Atemp = Ain.read();
		for(int k=0;k<VDATA_SIZE;k++){
			#pragma HLS UNROLL
			Ytemp.data[k]=alpha*Atemp.data[k]*Xtemp.data[k];
			//printf("%f ",Ytemp.data[k]);
		}
//		printf("A = %f",Ytemp.data[0]);
//		printf("\t");
		Y << Ytemp;
	}

}

void accum(hls::stream<float>& Yin,hls::stream<v_dt>& Ytemp,hls::stream<float>& Yout,float beta, int currCol,int M){

//	printf("\nExecute ACCUM \n");

	if(currCol==0){ //first iteration over columns

		for (int i=0;i<M;i++){
			#pragma HLS pipeline II=1
			float Yin_temp = beta*Yin.read();
			v_dt temp = Ytemp.read();
			for(int j=0;j<VDATA_SIZE;j++){
				#pragma HLS unroll
				Yin_temp+=temp.data[j];
//				printf("%f ",Yin_temp);
			}
//			printf("\t");
			Yout << Yin_temp;
		}

	}
	else{ //rest iteration over columns
		for (int i=0;i<M;i++){
			#pragma HLS pipeline II=1
			float Yin_temp = Yin.read();
			v_dt temp = Ytemp.read();
			for(int j=0;j<VDATA_SIZE;j++){
				#pragma HLS unroll
				Yin_temp+=temp.data[j];
//				printf("%f ",Yin_temp);
			}
//			printf("\t");
			Yout << Yin_temp;
		}
	}
}

extern "C" {
void krnl_gemv(const char TransA, const int M, const int N, const float alpha, v_dt  *Aupper1, v_dt  *Aupper2, v_dt *Alower1,  v_dt *Alower2,
		const int lda, v_dt  *X, const int incX, const float beta, float  *Yupper1, float  *Yupper2, float *Ylower1, float *Ylower2, const int incY) {

	#pragma HLS INTERFACE m_axi port = X offset = slave bundle = ddr0 //HBM 0
	#pragma HLS INTERFACE m_axi port = Aupper1 offset = slave bundle = ddr1 //HBM 1
	#pragma HLS INTERFACE m_axi port = Yupper1 offset = slave bundle = ddr2 //HBM 0
	#pragma HLS INTERFACE m_axi port = Alower1 offset = slave bundle = ddr3 //HBM 2
	#pragma HLS INTERFACE m_axi port = Ylower1 offset = slave bundle = ddr4 //HBM 0
	#pragma HLS INTERFACE m_axi port = Aupper2 offset = slave bundle = ddr5 //HBM 3
	#pragma HLS INTERFACE m_axi port = Yupper2 offset = slave bundle = ddr6 //HBM 0
	#pragma HLS INTERFACE m_axi port = Alower2 offset = slave bundle = ddr7 //HBM 4
	#pragma HLS INTERFACE m_axi port = Ylower2 offset = slave bundle = ddr8 //HBM 0

	#pragma HLS INTERFACE s_axilite port = N
	#pragma HLS INTERFACE s_axilite port = M
	#pragma HLS INTERFACE s_axilite port = lda
	#pragma HLS INTERFACE s_axilite port = TransA
	#pragma HLS INTERFACE s_axilite port = incX
	#pragma HLS INTERFACE s_axilite port = incY
	#pragma HLS INTERFACE s_axilite port = alpha
	#pragma HLS INTERFACE s_axilite port = beta

	static hls::stream<v_dt> Aup1;
	static hls::stream<v_dt> Aup2;
	static hls::stream<v_dt> Alow1;
	static hls::stream<v_dt> Alow2;


	static hls::stream<float> Yup1_in;
	static hls::stream<v_dt> Yup1_temp;
	static hls::stream<float> Yup1_out;

	static hls::stream<float> Yup2_in;
	static hls::stream<v_dt> Yup2_temp;
	static hls::stream<float> Yup2_out;

	static hls::stream<float> Ylow1_in;
	static hls::stream<v_dt> Ylow1_temp;
	static hls::stream<float> Ylow1_out;

	static hls::stream<float> Ylow2_in;
	static hls::stream<v_dt> Ylow2_temp;
	static hls::stream<float> Ylow2_out;


	static hls::stream<v_dt> Xup1;
	static hls::stream<v_dt> Xup2;
	static hls::stream<v_dt> Xlow1;
	static hls::stream<v_dt> Xlow2;


	#pragma HLS STREAM variable = Aup1 depth = 2048
	#pragma HLS STREAM variable = Aup2 depth = 2048
	#pragma HLS STREAM variable = Alow1 depth = 2048
	#pragma HLS STREAM variable = Alow2 depth = 2048


	#pragma HLS STREAM variable = Yup1_in depth = 2048
	#pragma HLS STREAM variable = Yup1_temp depth = 2048
	#pragma HLS STREAM variable = Yup1_out depth = 2048

	#pragma HLS STREAM variable = Yup2_in depth = 2048
	#pragma HLS STREAM variable = Yup2_temp depth = 2048
	#pragma HLS STREAM variable = Yup2_out depth = 2048

	#pragma HLS STREAM variable = Ylow1_in depth = 2048
	#pragma HLS STREAM variable = Ylow1_temp depth = 2048
	#pragma HLS STREAM variable = Ylow1_out depth = 2048

	#pragma HLS STREAM variable = Ylow2_in depth = 2048
	#pragma HLS STREAM variable = Ylow2_temp depth = 2048
	#pragma HLS STREAM variable = Ylow2_out depth = 2048

	#pragma HLS STREAM variable = Xup1 depth = 2048
	#pragma HLS STREAM variable = Xup2 depth = 2048
	#pragma HLS STREAM variable = Xlow1 depth = 2048
	#pragma HLS STREAM variable = Xlow2 depth = 2048


    loop_over_cols:
    for (int j=0;j<N;j+=VDATA_SIZE){ //16
		#pragma HLS dataflow
    	wide_read_x(Xup1, Xup2, Xlow1, Xlow2, X, j/VDATA_SIZE);

    	read_y(Yup1_in,Yupper1,M/4);
    	read_y(Yup2_in,Yupper2,M/4);
    	read_y(Ylow1_in,Ylower1,M/4);
    	read_y(Ylow2_in,Ylower2,M/4);


		wide_read_matrix( Aupper1, Aup1, M/4, N/VDATA_SIZE, j/VDATA_SIZE);
		wide_read_matrix( Aupper2, Aup2, M/4, N/VDATA_SIZE, j/VDATA_SIZE);
		wide_read_matrix( Alower1, Alow1, M/4, N/VDATA_SIZE, j/VDATA_SIZE);
		wide_read_matrix( Alower2, Alow2, M/4, N/VDATA_SIZE, j/VDATA_SIZE);


		gemv(Aup1,Yup1_temp,Xup1,alpha,M/4);
		gemv(Aup2,Yup2_temp,Xup2,alpha,M/4);
		gemv(Alow1,Ylow1_temp,Xlow1,alpha,M/4);
		gemv(Alow2,Ylow2_temp,Xlow2,alpha,M/4);

		accum(Yup1_in,Yup1_temp,Yup1_out,beta,j/VDATA_SIZE,M/4);
		accum(Yup2_in,Yup2_temp,Yup2_out,beta,j/VDATA_SIZE,M/4);
		accum(Ylow1_in,Ylow1_temp,Ylow1_out,beta,j/VDATA_SIZE,M/4);
		accum(Ylow2_in,Ylow2_temp,Ylow2_out,beta,j/VDATA_SIZE,M/4);

		write_y(Yup1_out,Yupper1,M/4);
		write_y(Yup2_out,Yupper2,M/4);
		write_y(Ylow1_out,Ylower1,M/4);
		write_y(Ylow2_out,Ylower2,M/4);

		}
    }
}


