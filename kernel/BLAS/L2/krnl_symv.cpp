#include "../../include/global.hpp"
#include "../../include/common.hpp"
#define block_size 2 // maybe 32 we'll see


const void symv(hls::stream<float>& Ain,float *x_b,const int j,float temp1, float temp2,hls::stream<float>& Ytemp) {
    float item;
	excecute_2:
		for(int i = 0; i< j;i++){
		#pragma HLS pipeline II=1
			item= Ain.read();
			Ytemp<< (temp1*item);
			temp2+=item*x_b[i];
		}
		Ytemp<<(temp1*Ain.read()+temp2);

}

//layout is for specific FPGA to check available sizes
extern "C" {
void krnl_symv (const char Uplo, const int N, const float alpha, const float  *A, const int lda,
                 const float  *X, const int incX, const float beta, float  *Y, const int incY){

#pragma HLS INTERFACE m_axi port = X offset = slave bundle = ddr0
#pragma HLS INTERFACE m_axi port = Y offset = slave bundle = ddr0
#pragma HLS INTERFACE m_axi port = A offset = slave bundle = ddr1

#pragma HLS INTERFACE s_axilite port = N
#pragma HLS INTERFACE s_axilite port = lda
#pragma HLS INTERFACE s_axilite port = Uplo
#pragma HLS INTERFACE s_axilite port = incX
#pragma HLS INTERFACE s_axilite port = incY
#pragma HLS INTERFACE s_axilite port = alpha
#pragma HLS INTERFACE s_axilite port = beta


static hls::stream<float> Aintop;
static hls::stream<float> Ainbottom;
static hls::stream<float> Ytemptop;
static hls::stream<float> Ytempbottom;
static hls::stream<float> Ytemp1;
static hls::stream<float> Ytemp2;
static hls::stream<float> Ytemp3;
static hls::stream<float> Ytemp4;
static hls::stream<float> Ain1;
static hls::stream<float> Ain2;
static hls::stream<float> Ain3;
static hls::stream<float> Ain4;
static hls::stream<float> Yout1;
static hls::stream<float> Yout2;
static hls::stream<float> Yout3;
static hls::stream<float> Yout4;
static hls::stream<float> Ysymm_top;
static hls::stream<float> Ysymm_out_top;
static hls::stream<float> Ysymm_bottom;
static hls::stream<float> Ysymm_out_bottom;

#pragma HLS STREAM variable = Ysymm_out_bottom depth = 2048
#pragma HLS STREAM variable = Ysymm_bottom depth = 2048
#pragma HLS STREAM variable = Ysymm_out_top depth = 2048
#pragma HLS STREAM variable = Ysymm_top depth = 2048
#pragma HLS STREAM variable = Yout4 depth = 2048
#pragma HLS STREAM variable = Yout3 depth = 2048
#pragma HLS STREAM variable = Yout1 depth = 2048
#pragma HLS STREAM variable = Yout2 depth = 2048
#pragma HLS STREAM variable = Ain3 depth = 2048
#pragma HLS STREAM variable = Ain4 depth = 2048
#pragma HLS STREAM variable = Ain1 depth = 2048
#pragma HLS STREAM variable = Ain2 depth = 2048
#pragma HLS STREAM variable = Ytemp1 depth = 2048
#pragma HLS STREAM variable = Ytemp2 depth = 2048
#pragma HLS STREAM variable = Ytemp3 depth = 2048
#pragma HLS STREAM variable = Ytemp4 depth = 2048
#pragma HLS STREAM variable = Ytemptop depth = 2048
#pragma HLS STREAM variable = Ytempbottom depth = 2048
#pragma HLS STREAM variable = Ainbottom depth = 2048
#pragma HLS STREAM variable = Aintop depth = 2048
int item;
int index1=0,index2=0;
float x_b[block_size];
float x_b_end[block_size];
float x_b_change[block_size];
float x_b_change_end[block_size];
int times=N/block_size;
int new_j;

    if(Uplo=='L' or Uplo=='l' ){
    }
    else if(Uplo=='U' or Uplo=='u' ){
    #pragma HLS dataflow
    	main_loop:
    	for (int j=0;j<times/2;j++){
			/**
			 * @brief top symv
			 *
			 */
    		loop_top:
			index1=j*block_size*incX;
			index1=set_x_b(block_size,index1,X,incX,x_b,alpha);
			read_vector_symmetric_matrix(A, Aintop,  (N+1)*j*block_size, block_size,Uplo,N);
			symv_loop_top:
			for (int k=0; k<block_size;k++){
				read_vector_matrix(Y, Ysymm_top,j*block_size,(j*block_size)+k+1,incY);
				symv(Aintop,x_b,k,x_b[k],0,Ytemptop);
				add_streams_to_stream_N_items(Ysymm_top,Ytemptop,Ysymm_out_top,k+1);
				write_multiple_items_from_fifo(Y,Ysymm_out_top,j*block_size,(j*block_size)+k+1,incY);
			}

			/**
			 * @brief top rest
			 *
			 */
			loop_top_gemv:
			for (int i=(j+1);i<times;i++){
				index1=i*block_size*incX;
				index1=set_x_b(block_size,index1,X,incX,x_b_change,alpha);
				read_vector_matrix(Y, Yout1,j*block_size,(j+1)*block_size,incY);
				read_vector_matrix(Y, Yout2,i*block_size,(i+1)*block_size,incY);
				/**
				 * read normal -> j-j+1
				 */
				read_matrix_tile(A, Ain1, (j*N+i)*block_size, block_size,block_size,N);
				/**
				 * read transpose -> i-i+1
				 */
				read_matrix_tile_transpose(A, Ain2, (j*N+i)*block_size, block_size,block_size,N);
				gemv_block(Ain1,x_b_change,block_size,block_size,Ytemp1);
				gemv_block(Ain2,x_b,block_size,block_size,Ytemp2);
				add_streams_to_stream_N_items(Yout1,Ytemp1,Ain1,block_size);
				add_streams_to_stream_N_items(Yout2,Ytemp2,Ain2,block_size);

				write_multiple_items_from_fifo(Y,Ain1,j*block_size,(j+1)*block_size,incY);
				write_multiple_items_from_fifo(Y,Ain2,i*block_size,(i+1)*block_size,incY);
			}
			/**
			 * @brief bottom symv
			 *
			 */
    		loop_bottom:
			new_j=times-1-j;
			index2=new_j*block_size*incX;
			index2=set_x_b(block_size,index2,X,incX,x_b_end,alpha);
			read_vector_symmetric_matrix(A, Ainbottom,  (N+1)*new_j*block_size, block_size,Uplo,N);
			symv_loop_bottom:
			for (int k=0; k<block_size;k++){
				read_vector_matrix(Y, Ysymm_bottom,new_j*block_size,(new_j*block_size)+k+1,incY);
				symv(Ainbottom,x_b_end,k,x_b_end[k],0,Ytempbottom);
				add_streams_to_stream_N_items(Ysymm_bottom,Ytempbottom,Ysymm_out_bottom,k+1);
				write_multiple_items_from_fifo(Y,Ysymm_out_bottom,new_j*block_size,(new_j*block_size)+k+1,incY);
			}

			/**
			 * @brief bottom rest
			 *
			 */
			loop_bottom_gemv:
			for (int i=new_j+1;i<times;i++){
				index2=i*block_size*incX;
				index2=set_x_b(block_size,index2,X,incX,x_b_change_end,alpha);
				read_vector_matrix(Y, Yout3,new_j*block_size,(new_j+1)*block_size,incY);
				read_vector_matrix(Y, Yout4,i*block_size,(i+1)*block_size,incY);
				/**
				 * read normal -> j-j+1
				 */
				read_matrix_tile(A, Ain3, (new_j*N+i)*block_size, block_size,block_size,N);
				/**
				 * read transpose -> i-i+1
				 */
				read_matrix_tile_transpose(A, Ain4, (new_j*N+i)*block_size, block_size,block_size,N);
				gemv_block(Ain3,x_b_change_end,block_size,block_size,Ytemp3);
				gemv_block(Ain4,x_b_end,block_size,block_size,Ytemp4);
				add_streams_to_stream_N_items(Yout3,Ytemp3,Ain3,block_size);
				add_streams_to_stream_N_items(Yout4,Ytemp4,Ain4,block_size);

				write_multiple_items_from_fifo(Y,Ain3,new_j*block_size,(new_j+1)*block_size,incY);
				write_multiple_items_from_fifo(Y,Ain4,i*block_size,(i+1)*block_size,incY);
			}
    }
}

}}
