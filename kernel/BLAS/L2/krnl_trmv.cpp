#include <hls_stream.h>
#include <ap_int.h>
#include <hls_math.h>
#include <stdio.h>


#define VDATA_SIZE 16
typedef struct v_datatype { float data[VDATA_SIZE]; } v_dt;

inline void wide_read_x(hls::stream<v_dt>& Xup, v_dt *X,int currCol){

	v_dt Xtemp;
	#pragma HLS pipeline II=1
	Xtemp=X[currCol];
	Xup << Xtemp;


}

inline void read_y(const char Uplo,hls::stream<float>& Yupper,float *Y,int currCol,int N){

	int start=0;
	int end=(currCol)*VDATA_SIZE;

	for(int i=start;i<end;i++){
		#pragma HLS pipeline II=1
		Yupper <<Y[i];
	}
}

inline void write_y(const char Uplo,hls::stream<float>& Yupper,float *Y,int currCol,int N){
	int start = 0;
	int end = (currCol+1)*VDATA_SIZE;

	for(int i=start;i<end;i++){
		#pragma HLS pipeline II=1
		Y[i] = Yupper.read();
	}
}

inline void wide_read_matrix(const char Uplo,v_dt *A, hls::stream<v_dt>& AStream, int N, int currCol){
	int start =0;
	int end =(currCol+1)*VDATA_SIZE;

	int row=N/VDATA_SIZE;
	int col=currCol;


	wide_read:
	for(int i=start;i<end;i++){
		#pragma HLS pipeline II=1
		AStream << A[i*row+col];
	}
}

inline void gemv(const char Uplo,hls::stream<v_dt>& Ain, hls::stream<v_dt>& Y,hls::stream<v_dt>& X, int currCol,int N){

	v_dt Xtemp=X.read();
	v_dt Ytemp;
	v_dt Atemp;
	int limit = currCol*VDATA_SIZE;

	gemv_trig:
	for(int i=0;i<limit+VDATA_SIZE;i++){
		#pragma HLS pipeline II=1
		Atemp = Ain.read();
		int start= i-limit;
		int end = VDATA_SIZE;

		for(int k=0;k<VDATA_SIZE;k++){
			#pragma HLS UNROLL
			if (i<limit){
				Ytemp.data[k]=Atemp.data[k]*Xtemp.data[k];
			}
			else {
				if(k>=start){
					Ytemp.data[k]=Atemp.data[k]*Xtemp.data[k];
				}
				else{
					Ytemp.data[k]=0;
				}
			}
		}
		Y << Ytemp;
	}

}

inline void accum(const char Uplo,hls::stream<float>& Yin,hls::stream<v_dt>& Ytemp,hls::stream<float>& Yout, int currCol,int M){

	int limit = currCol*VDATA_SIZE;

	for (int i=0;i<limit+VDATA_SIZE;i++){
		#pragma HLS pipeline II=1
		float Yin_temp;
		v_dt temp = Ytemp.read();
		if (i<limit){
			Yin_temp= Yin.read();
		}
		else{
			Yin_temp=0;
		}
		for(int j=0;j<VDATA_SIZE;j++){
			#pragma HLS unroll
			Yin_temp=Yin_temp+temp.data[j];
		}
		Yout << Yin_temp;
	}
}

extern "C" {
void krnl_trmv(const char Uplo, const char TransA, const char Diag,
                 const int N, const float  *A, const int lda,float  *X, const int incX,float *Y){

#pragma HLS INTERFACE m_axi port = X offset = slave bundle = ddr0  // HBM0
#pragma HLS INTERFACE m_axi port = A offset = slave bundle = ddr1	// HBM1
#pragma HLS INTERFACE m_axi port = Y offset = slave bundle = ddr2	//HBM2

#pragma HLS INTERFACE s_axilite port = N
#pragma HLS INTERFACE s_axilite port = lda
#pragma HLS INTERFACE s_axilite port = TransA
#pragma HLS INTERFACE s_axilite port = incX
#pragma HLS INTERFACE s_axilite port = Diag
#pragma HLS INTERFACE s_axilite port = Uplo

static hls::stream<v_dt> Aup;

static hls::stream<float> Yup_in;
static hls::stream<v_dt> Yup_temp;
static hls::stream<float> Yup_out;

static hls::stream<v_dt> Xup;


#pragma HLS STREAM variable = Aup depth = 2048

#pragma HLS STREAM variable = Yup_in depth = 2048
#pragma HLS STREAM variable = Yup_temp depth = 2048
#pragma HLS STREAM variable = Yup_out depth = 2048

#pragma HLS STREAM variable = Xup depth = 2048

loop_over_cols:
for (int j=0;j<N/VDATA_SIZE;j++){ //16
	#pragma HLS dataflow
	wide_read_x(Xup,(v_dt*)X,j);

	read_y(Uplo,Yup_in,Y,j,N);

	wide_read_matrix( Uplo, (v_dt*)A, Aup, N, j);

	gemv(Uplo,Aup,Yup_temp,Xup,j,N);

	accum(Uplo,Yup_in,Yup_temp,Yup_out,j,N);

	write_y(Uplo,Yup_out,Y,j,N);


	}


}}
