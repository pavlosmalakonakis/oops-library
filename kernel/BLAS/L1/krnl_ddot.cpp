#include "../../include/global.hpp"
#include "../../include/common.hpp"

static void ddot(hls::stream< v_dt>& Xin, hls::stream< v_dt>& Yin,const int N,float* result, const float alpha) {

	double temp_result[VDATA_SIZE];
	#pragma HLS ARRAY_PARTITION temp_result dim=1 complete
	double final_result = 0;

    v_dt x,y;

    init_ddot:
	for(int i=0;i<VDATA_SIZE;i++){
		#pragma HLS unroll
		temp_result[i]=0;
	}
execute:
	for (int i = 0; i < N; i+=VDATA_SIZE) {
	#pragma HLS pipeline II=1
    	x=Xin.read();
        y=Yin.read();
		for(int j=0;j<VDATA_SIZE;j++){
		#pragma HLS unroll
			if(i+VDATA_SIZE<=N || j<(N%VDATA_SIZE)){
				temp_result[j] +=(x.data[j]*y.data[j]);
			}
			else{
				temp_result[j] +=0;
			}
		}
	}

excecute_final_of_ddot:
	for (int i=0;i<VDATA_SIZE;i++){
		#pragma HLS pipeline II=1
		final_result+=temp_result[i];
	}

	result[0] = alpha+final_result;

}


extern "C" {
void krnl_ddot(const int N,const float alpha, v_dt* X,const int incx, v_dt* Y, const int incy, float* result) {

#pragma HLS INTERFACE m_axi port = X offset = slave bundle = ddr0
#pragma HLS INTERFACE m_axi port = Y offset = slave bundle = ddr1

#pragma HLS INTERFACE s_axilite port = N
#pragma HLS INTERFACE s_axilite port = alpha
#pragma HLS INTERFACE s_axilite port = incx
#pragma HLS INTERFACE s_axilite port = incy
#pragma HLS INTERFACE s_axilite port = result

  static hls::stream<v_dt> Xin;
  static hls::stream<v_dt> Yin;
#pragma HLS STREAM variable = Xin depth = 128
#pragma HLS STREAM variable = Yin depth = 128

#pragma HLS dataflow
    read_vector_wide((v_dt*)X, Xin, N,incx);
    read_vector_wide((v_dt*)Y, Yin, N,incy);
    ddot(Xin,Yin,N,result,alpha);

}
}
