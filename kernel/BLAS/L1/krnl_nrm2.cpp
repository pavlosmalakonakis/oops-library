#include <math.h>
#include "../../include/global.hpp"
#include "../../include/common.hpp"

static void nrm2(hls::stream< v_dt>& Xin, const int N,float *final_result) {

	float result[VDATA_SIZE];
	float temp_result=0;
	#pragma HLS ARRAY_PARTITION result dim=1 complete
	v_dt temp;

	init_nrm2:
	for(int i=0;i<VDATA_SIZE;i++){
		#pragma HLS unroll
		result[i]=0;
	}

	excecute_nrm2:
	for (int i = 0; i < N; i+=VDATA_SIZE) {
	#pragma HLS pipeline II=1
    	temp=Xin.read();
		for(int j=0;j<VDATA_SIZE;j++){
			#pragma HLS unroll
			if(i+VDATA_SIZE<=N || j<(N%VDATA_SIZE)){
				result[j] +=(temp.data[j]*temp.data[j]);
			}
			else{
				result[j] +=0;
			}
		}
	}

	excecute_final_of_nrm2:
	for (int i=0;i<VDATA_SIZE;i++){
		#pragma HLS pipeline II=1
		temp_result+=result[i];
	}

	final_result[0]=sqrtf(temp_result);
}

extern "C" {
void krnl_nrm2(const int N,v_dt* X,const int incx,float* result) {
#pragma HLS INTERFACE m_axi port = X offset = slave bundle = ddr0

#pragma HLS INTERFACE s_axilite port = N
#pragma HLS INTERFACE s_axilite port = incx
#pragma HLS INTERFACE s_axilite port = result

  static hls::stream<v_dt> Xin;
#pragma HLS STREAM variable = Xin depth = 128


#pragma HLS dataflow
    read_vector_wide((v_dt*)X, Xin, N,incx);
	nrm2(Xin,N,result);


}
}
