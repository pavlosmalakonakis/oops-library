#include "../include/global.hpp"

extern "C"
{
  void krnl_lu(float *A, float *L, float *U, int n)
  {

#pragma HLS INTERFACE m_axi port = A offset = slave bundle = ddr0
#pragma HLS INTERFACE m_axi port = L offset = slave bundle = ddr0
#pragma HLS INTERFACE m_axi port = U offset = slave bundle = ddr0
#pragma HLS INTERFACE s_axilite port = n

    //perform LU decomposition

	  // change loop order first j then i , also for the lower part.
	  // makes it easy to use consecutive memory


    int i = 0, j = 0, k = 0;
    for (i = 0; i < n; i++)
    {
      for (j = 0; j < n; j++)
      {
        if (j < i)
          L[j * n + i] = 0;
        else
        {
          L[j * n + i] = A[j * n + i];
          for (k = 0; k < i; k++)
          {
            L[j * n + i] = L[j * n + i] - L[j * n + k] * U[k * n + i];
          }
        }
      }
      for (j = 0; j < n; j++)
      {
        if (j < i)
          U[i * n + j] = 0;
        else if (j == i)
          U[i * n + j] = 1;
        else
        {
          U[i * n + j] = A[i * n + j] / L[i * n + i];
          for (k = 0; k < i; k++)
          {
            U[i * n + j] = U[i * n + j] - ((L[i * n + k] * U[k * n + j]) / L[i * n + i]);
          }
        }
      }
    }
  }
}
