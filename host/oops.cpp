#include "oops.hpp"
#include <chrono>
#include <ctime>
#include <ratio>
using namespace std::chrono;
std::chrono::time_point<std::chrono::system_clock> start, ending;
// common functions

void *OOPS_malloc(size_t alloc_bytes){
	void *X;

	auto align_sz = sysconf(_SC_PAGESIZE);
	X= (void *)aligned_alloc(align_sz,alloc_bytes);

	return X;
}

void program_device(const char *arg1){

	cl_int err;

	//programm_device(arg1,&err,&context, &q, &krnl,krnl_name);
	auto binaryFile = arg1;
	// OPENCL HOST CODE AREA START
	auto devices = xcl::get_xil_devices();

	// Create Program and Kernel
	auto fileBuf = xcl::read_binary_file(binaryFile);
	cl::Program::Binaries bins{{fileBuf.data(), fileBuf.size()}};
	bool valid_device = false;
	for (unsigned int i = 0; i < devices.size(); i++) {
		auto device = devices[i];
	   // Creating Context and Command Queue for selected Device
	   OCL_CHECK(err, context = cl::Context(device, nullptr, nullptr, nullptr, &err));
	   OCL_CHECK(err, q = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err));

	   std::cout << "Trying to program device[" << i << "]: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
	   program=cl::Program (context, {device}, bins, nullptr, &err);

	   if (err != CL_SUCCESS) {
				std::cout << "Failed to program device[" << i << "] with xclbin file!\n";
	   } else {
				std::cout << "Device[" << i << "]: program successful!\n";
				valid_device = true;
				break; // we break because we found a valid device
	   }
	}

	if (!valid_device) {
		std::cout << "Failed to program any device found, exit!\n";
		exit(EXIT_FAILURE);
	}
}

void release_device(){

	clReleaseProgram(program.get());
	clReleaseContext(context.get());
	clReleaseCommandQueue(q.get());

}

// L1 functions

void OOPS_copy( const int N, const float *X, const int incX, float *Y, const int incY){
		cl_int err;

		size_t input_vector_size_bytes = sizeof(float) * N*incX;
		size_t output_vector_size_bytes = sizeof(float) * N*incY;

		cl::Kernel krnl;

		OCL_CHECK(err, krnl= cl::Kernel (program,"krnl_copy", &err));

		// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, input_vector_size_bytes,
											   (void *) X, &err));
		OCL_CHECK(err, cl::Buffer Sy(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, output_vector_size_bytes,
												(void *) Y, &err));
		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, N));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
		OCL_CHECK(err, err = krnl.setArg(narg++, incX));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sy));
		OCL_CHECK(err, err = krnl.setArg(narg++, incY));

		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
		q.finish();
		// Launch the Kernel
		q.enqueueTask(krnl);
	   // OCL_CHECK(err, err = );

		q.finish();

		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, CL_MIGRATE_MEM_OBJECT_HOST));
		q.finish();

		clReleaseKernel(krnl.get());

}

float OOPS_ddot( const int N, const float alpha, const float *X, const int incX, const float *Y, const int incY){

		cl_int err;

		size_t input_vector_size_bytes = sizeof(float) * N*incX;
		size_t output_vector_size_bytes = sizeof(float) * N*incY;
		size_t result_size = sizeof(float);
		float *result_host =NULL;
		//std::cout<<*result_host <<" should be 0"<<std::endl;
		posix_memalign((void **)&result_host, 4096, sizeof(float));

		cl::Kernel krnl;

		OCL_CHECK(err, krnl= cl::Kernel (program,"krnl_ddot", &err));



		// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, input_vector_size_bytes,
											   (void *)X, &err));
		OCL_CHECK(err, cl::Buffer Sy(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, output_vector_size_bytes,
												(void *)Y, &err));
		OCL_CHECK(err, cl::Buffer result(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, result_size,
														result_host, &err));

		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, N));
		OCL_CHECK(err, err = krnl.setArg(narg++, alpha));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
		OCL_CHECK(err, err = krnl.setArg(narg++, incX));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sy));
		OCL_CHECK(err, err = krnl.setArg(narg++, incY));
		OCL_CHECK(err, err = krnl.setArg(narg++, result));

		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, 0 /* 0 means from host*/));
		// Launch the Kernel
		OCL_CHECK(err, err = q.enqueueTask(krnl));
		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({result}, CL_MIGRATE_MEM_OBJECT_HOST));
		q.finish();
		clReleaseKernel(krnl.get());

		return *result_host;

}

float OOPS_dot( const int N, const float *X, const int incX, const float *Y, const int incY){

		cl_int err;

		size_t input_vector_size_bytes = sizeof(float) * N*incX;
		size_t output_vector_size_bytes = sizeof(float) * N*incY;
		float *result_host =NULL;
		//std::cout<<*result_host <<" should be 0"<<std::endl;
		posix_memalign((void **)&result_host, 4096, sizeof(float));

		cl::Kernel krnl;

		OCL_CHECK(err, krnl= cl::Kernel (program,"krnl_dot", &err));

		// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, input_vector_size_bytes,
											   (void *) X, &err));
		OCL_CHECK(err, cl::Buffer Sy(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, output_vector_size_bytes,
												(void *) Y, &err));
		OCL_CHECK(err, cl::Buffer result(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, sizeof(float),
														result_host, &err));

		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, N));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
		OCL_CHECK(err, err = krnl.setArg(narg++, incX));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sy));
		OCL_CHECK(err, err = krnl.setArg(narg++, incY));
		OCL_CHECK(err, err = krnl.setArg(narg++, result));

		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, 0 /* 0 means from host*/));
		// Launch the Kernel
		OCL_CHECK(err, err = q.enqueueTask(krnl));
		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({result}, CL_MIGRATE_MEM_OBJECT_HOST));
		q.finish();

		clReleaseKernel(krnl.get());

		return *result_host;

}

void OOPS_rot( const int N, float *X, const int incX, float *Y, const int incY, const float  c, const float  s){

	cl_int err;

	size_t input_vector_size_bytes = sizeof(float) * N*incX;
	size_t output_vector_size_bytes = sizeof(float) * N*incY;

	cl::Kernel krnl;

	OCL_CHECK(err, krnl= cl::Kernel (program, "krnl_rot", &err));

	// Allocate Buffer in Global Memory
	OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, input_vector_size_bytes,
										   (void*) X, &err));
	OCL_CHECK(err, cl::Buffer Sy(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, output_vector_size_bytes,
											(void *) Y, &err));
	// Set the Kernel Arguments
	int narg = 0;
	OCL_CHECK(err, err = krnl.setArg(narg++, N));
	OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
	OCL_CHECK(err, err = krnl.setArg(narg++, incX));
	OCL_CHECK(err, err = krnl.setArg(narg++, Sy));
	OCL_CHECK(err, err = krnl.setArg(narg++, incY));
	OCL_CHECK(err, err = krnl.setArg(narg++, c));
	OCL_CHECK(err, err = krnl.setArg(narg++, s));

	// Copy input data to device global memory
	OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
	OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, 0 /* 0 means from host*/));
	// Launch the Kernel
	OCL_CHECK(err, err = q.enqueueTask(krnl));

	// Copy Result from Device Global Memory to Host Local Memory
	OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, CL_MIGRATE_MEM_OBJECT_HOST));
	OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, CL_MIGRATE_MEM_OBJECT_HOST));
	q.finish();

	clReleaseKernel(krnl.get());

}

void OOPS_rotm( const int N, float* X, const int incX, float* Y, const int incY, const float* P){


	cl_int err;
	size_t input_vector_size_bytes = sizeof(float) * N*incX;
	size_t output_vector_size_bytes = sizeof(float) * N*incY;

	cl::Kernel krnl;

	OCL_CHECK(err, krnl= cl::Kernel (program, "krnl_rotm", &err));

	// Allocate Buffer in Global Memory
	OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, input_vector_size_bytes,
										   (void*) X, &err));
	OCL_CHECK(err, cl::Buffer Sy(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, output_vector_size_bytes,
											(void *) Y, &err));
	OCL_CHECK(err, cl::Buffer SP(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, sizeof(float) *5,
											(void *) P, &err));
	start = std::chrono::system_clock::now();
	// Set the Kernel Arguments
	int narg = 0;
	OCL_CHECK(err, err = krnl.setArg(narg++, N));
	OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
	OCL_CHECK(err, err = krnl.setArg(narg++, incX));
	OCL_CHECK(err, err = krnl.setArg(narg++, Sy));
	OCL_CHECK(err, err = krnl.setArg(narg++, incY));
	OCL_CHECK(err, err = krnl.setArg(narg++, SP));

	// Copy input data to device global memory
	OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
	OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, 0 /* 0 means from host*/));
	OCL_CHECK(err, err = q.enqueueMigrateMemObjects({SP}, 0 /* 0 means from host*/));
	// Launch the Kernel
	OCL_CHECK(err, err = q.enqueueTask(krnl));

	// Copy Result from Device Global Memory to Host Local Memory
	OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, CL_MIGRATE_MEM_OBJECT_HOST));
	OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, CL_MIGRATE_MEM_OBJECT_HOST));
	q.finish();

	clReleaseKernel(krnl.get());
}

float OOPS_nrm2( const int N, const float *X, const int incX){

		cl_int err;
		size_t input_vector_size_bytes = sizeof(float) * N*incX;

		size_t result_size = sizeof(float);
		float *result_host =NULL;
		//std::cout<<*result_host <<" should be 0"<<std::endl;
		posix_memalign((void **)&result_host, 4096, sizeof(float));


		cl::Kernel krnl;

		OCL_CHECK(err, krnl= cl::Kernel (program, "krnl_nrm2", &err));

				// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, input_vector_size_bytes,
													   (void *)X, &err));
		OCL_CHECK(err, cl::Buffer result(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY,result_size,
														result_host, &err));

		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, N));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
		OCL_CHECK(err, err = krnl.setArg(narg++, incX));
		OCL_CHECK(err, err = krnl.setArg(narg++, result));

		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
		// Launch the Kernel
		OCL_CHECK(err, err = q.enqueueTask(krnl));
		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({result}, CL_MIGRATE_MEM_OBJECT_HOST));
		q.finish();

		clReleaseKernel(krnl.get());
		return *result_host;

}


void OOPS_scal( const int N, const float alpha, float *X, const int incX){

	cl_int err;

	size_t input_vector_size_bytes = sizeof(float) * N*incX;

	cl::Kernel krnl;

	OCL_CHECK(err, krnl= cl::Kernel (program, "krnl_scal", &err));


	// Allocate Buffer in Global Memory
	OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, input_vector_size_bytes,
										   (void *)X, &err));

	// Set the Kernel Arguments
	int narg = 0;
	OCL_CHECK(err, err = krnl.setArg(narg++, N));
	OCL_CHECK(err, err = krnl.setArg(narg++, alpha));
	OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
	OCL_CHECK(err, err = krnl.setArg(narg++, incX));

	// Copy input data to device global memory
	OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
	// Launch the Kernel
	OCL_CHECK(err, err = q.enqueueTask(krnl));

	// Copy Result from Device Global Memory to Host Local Memory
	OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, CL_MIGRATE_MEM_OBJECT_HOST));
	q.finish();

	clReleaseKernel(krnl.get());

}

void OOPS_swap( const int N, float *X, const int incX, float *Y, const int incY){

		cl_int err;


		size_t input_vector_size_bytes = sizeof(float) * N*incX;
		size_t output_vector_size_bytes = sizeof(float) * N*incY;

		cl::Kernel krnl;

		OCL_CHECK(err, krnl= cl::Kernel (program,"krnl_swap", &err));

		// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, input_vector_size_bytes,
											   (void *)X, &err));
		OCL_CHECK(err, cl::Buffer Sy(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, output_vector_size_bytes,
												(void *)Y, &err));
		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, N));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
		OCL_CHECK(err, err = krnl.setArg(narg++, incX));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sy));
		OCL_CHECK(err, err = krnl.setArg(narg++, incY));

		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, 0 /* 0 means from host*/));
		// Launch the Kernel
		OCL_CHECK(err, err = q.enqueueTask(krnl));
		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, CL_MIGRATE_MEM_OBJECT_HOST));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, CL_MIGRATE_MEM_OBJECT_HOST));
		q.finish();

		clReleaseKernel(krnl.get());
}

void OOPS_axpy(const int N, const float alpha, const float *X, const int incX, float *Y, const int incY){
		cl_int err;

		size_t input_vector_size_bytes = sizeof(float) * N*incX;
		size_t output_vector_size_bytes = sizeof(float) * N*incY;

		cl::Kernel krnl;

		OCL_CHECK(err, krnl= cl::Kernel (program,"krnl_axpy", &err));


		// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, input_vector_size_bytes,
											   (void *) X, &err));
		OCL_CHECK(err, cl::Buffer Sy(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, output_vector_size_bytes,
												(void *)Y, &err));

		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, N));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
		OCL_CHECK(err, err = krnl.setArg(narg++, incX));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sy));
		OCL_CHECK(err, err = krnl.setArg(narg++, incY));
		OCL_CHECK(err, err = krnl.setArg(narg++, alpha));


		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, 0 /* 0 means from host*/));

		// Launch the Kernel
		OCL_CHECK(err, err = q.enqueueTask(krnl));
		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, CL_MIGRATE_MEM_OBJECT_HOST));
		q.finish();

		clReleaseKernel(krnl.get());
}

void OOPS_xpay(const int N, const float alpha, const float *X, const int incX, float *Y, const int incY){

	   cl_int err;

		size_t input_vector_size_bytes = sizeof(float) * N*incX;
		size_t output_vector_size_bytes = sizeof(float) * N*incY;

		cl::Kernel krnl;

		OCL_CHECK(err, krnl= cl::Kernel (program,"krnl_xpay", &err));

		// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, input_vector_size_bytes,
											   (void *) X, &err));
		OCL_CHECK(err, cl::Buffer Sy(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, output_vector_size_bytes,
												(void *)Y, &err));
		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, N));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
		OCL_CHECK(err, err = krnl.setArg(narg++, incX));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sy));
		OCL_CHECK(err, err = krnl.setArg(narg++, incY));
		OCL_CHECK(err, err = krnl.setArg(narg++, alpha));


		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, 0 /* 0 means from host*/));

		// Launch the Kernel
		OCL_CHECK(err, err = q.enqueueTask(krnl));
		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, CL_MIGRATE_MEM_OBJECT_HOST));
		q.finish();

		clReleaseKernel(krnl.get());


}

float OOPS_asum(const int N, const float *X, const int incX){

		cl_int err;

		size_t input_vector_size_bytes = sizeof(float) * N*incX;
		float *result_host =NULL;
		posix_memalign((void **)&result_host, 4096, sizeof(float));

		cl::Kernel krnl;

		OCL_CHECK(err, krnl= cl::Kernel (program,"krnl_asum", &err));

		// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, input_vector_size_bytes,
											   (void *) X, &err));
		OCL_CHECK(err, cl::Buffer result(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, sizeof(double),
														result_host, &err));

		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, N));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
		OCL_CHECK(err, err = krnl.setArg(narg++, incX));
		OCL_CHECK(err, err = krnl.setArg(narg++, result));

		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
		// Launch the Kernel
		OCL_CHECK(err, err = q.enqueueTask(krnl));
		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({result}, CL_MIGRATE_MEM_OBJECT_HOST));
		q.finish();
		clReleaseKernel(krnl.get());
		return *result_host;

}

int OOPS_iamax(const int N, const float  *X, const int incX){
		cl_int err;


		size_t input_vector_size_bytes = sizeof(float) * N*incX;
		int *result_host =NULL;
		posix_memalign((void **)&result_host, 4096, sizeof(int));

		cl::Kernel krnl;

		OCL_CHECK(err, krnl= cl::Kernel (program,"krnl_iamax", &err));

		// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, input_vector_size_bytes,
											   (void *) X, &err));
		OCL_CHECK(err, cl::Buffer result(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, sizeof(int),
														result_host, &err));
		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, N));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
		OCL_CHECK(err, err = krnl.setArg(narg++, incX));
		OCL_CHECK(err, err = krnl.setArg(narg++, result));

		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
		// Launch the Kernel
		OCL_CHECK(err, err = q.enqueueTask(krnl));
		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({result}, CL_MIGRATE_MEM_OBJECT_HOST));
		q.finish();

		clReleaseKernel(krnl.get());

		return *result_host;

}

//L2 functions

void OOPS_gemv(const char TransA, const int M, const int N, const float alpha, const float *A, const int lda,
				 const float  *X, const int incX, const float beta, float  *Y, const int incY){

		cl_int err;
		int row_size= M;
		int col_size =N;
		if(TransA=='C'|| TransA=='T'){
			row_size =N;
			col_size =M;
		}

		size_t input_vector_size_bytes = sizeof(float) * col_size*incX;
		size_t output_vector_size_bytes = sizeof(float) * (row_size/4)*incY;
		size_t matrix_size_bytes = sizeof(float) *N*(M/4);

		cl::Kernel krnl;
		OCL_CHECK(err, krnl= cl::Kernel (program,"krnl_gemv", &err));

		// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, input_vector_size_bytes,
											   (void *) X, &err));

		OCL_CHECK(err, cl::Buffer Sy_up1(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, output_vector_size_bytes,
												(void *) Y, &err));

		OCL_CHECK(err, cl::Buffer Sy_up2(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, output_vector_size_bytes,
														(void *) &Y[M/4], &err));

		OCL_CHECK(err, cl::Buffer Sy_low1(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, output_vector_size_bytes,
														(void *) &Y[2*M/4], &err));

		OCL_CHECK(err, cl::Buffer Sy_low2(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, output_vector_size_bytes,
																(void *) &Y[3*M/4], &err));

		OCL_CHECK(err, cl::Buffer Sa_up1(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, matrix_size_bytes,
														 (void *) A, &err));

		OCL_CHECK(err, cl::Buffer Sa_up2(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, matrix_size_bytes,
														 (void *) &A[(M/4)*N], &err));

		OCL_CHECK(err, cl::Buffer Sa_low1(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, matrix_size_bytes,
																 (void *) &A[(2*M/4)*N], &err));

		OCL_CHECK(err, cl::Buffer Sa_low2(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, matrix_size_bytes,
																 (void *) &A[(3*M/4)*N], &err));

		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, TransA));
		OCL_CHECK(err, err = krnl.setArg(narg++, M));
		OCL_CHECK(err, err = krnl.setArg(narg++, N));
		OCL_CHECK(err, err = krnl.setArg(narg++, alpha));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sa_up1));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sa_up2));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sa_low1));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sa_low2));
		OCL_CHECK(err, err = krnl.setArg(narg++, lda));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
		OCL_CHECK(err, err = krnl.setArg(narg++, incX));
		OCL_CHECK(err, err = krnl.setArg(narg++, beta));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sy_up1));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sy_up2));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sy_low1));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sy_low2));
		OCL_CHECK(err, err = krnl.setArg(narg++, incY));

		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sa_up1}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sa_up2}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sa_low1}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sa_low2}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy_up1}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy_up2}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy_low1}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy_low2}, 0 /* 0 means from host*/));


		// Launch the Kernel
		OCL_CHECK(err, err = q.enqueueTask(krnl));
		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy_up1}, CL_MIGRATE_MEM_OBJECT_HOST));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy_up2}, CL_MIGRATE_MEM_OBJECT_HOST));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy_low1}, CL_MIGRATE_MEM_OBJECT_HOST));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy_low2}, CL_MIGRATE_MEM_OBJECT_HOST));


		q.finish();
		clReleaseKernel(krnl.get());

}

void OOPS_lu( float *A, float *L, float *U, int N){
		cl_int err;

		size_t input_matrix_A_size_bytes = sizeof(float) * N*N;
		size_t output_matrix_L_size_bytes = sizeof(float) * N*N;
		size_t output_matrix_U_size_bytes = sizeof(float) * N*N;


		cl::Kernel krnl;

		OCL_CHECK(err, krnl= cl::Kernel (program,"krnl_lu", &err));

		// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sa(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, input_matrix_A_size_bytes,
											   (void *) A, &err));
		OCL_CHECK(err, cl::Buffer Sl(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, output_matrix_L_size_bytes,
												(void *) L, &err));
		OCL_CHECK(err, cl::Buffer Su(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, output_matrix_U_size_bytes,
														(void *) U, &err));

		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, Sa));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sl));
		OCL_CHECK(err, err = krnl.setArg(narg++, Su));
		OCL_CHECK(err, err = krnl.setArg(narg++, N));


		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sa}, 0 /* 0 means from host*/));
		// Launch the Kernel
		OCL_CHECK(err, err = q.enqueueTask(krnl));
		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sl}, CL_MIGRATE_MEM_OBJECT_HOST));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Su}, CL_MIGRATE_MEM_OBJECT_HOST));
		q.finish();
		clReleaseKernel(krnl.get());

}


void OOPS_trmv(const char Uplo, const char TransA, const char Diag,
				 const int N, const float  *A, const int lda, float  *X, const int incX){
	 cl_int err;

		size_t input_vector_size_bytes = sizeof(float) * N*incX;
		size_t matrix_size_bytes = sizeof(float) *N*N;
		size_t output_matrix_size_bytes = sizeof(float) *N;
		float *Y;
		Y= (float *)OOPS_malloc(sizeof(float)*N);
		cl::Kernel krnl;
		OCL_CHECK(err, krnl= cl::Kernel (program,"krnl_trmv", &err));

		// Allocate Buffer in Global Memory
		OCL_CHECK(err, cl::Buffer Sx(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, input_vector_size_bytes,
											   (void *) X, &err));
		OCL_CHECK(err, cl::Buffer Sa(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, matrix_size_bytes,
														 (void *) A, &err));
		OCL_CHECK(err, cl::Buffer Sy(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, output_matrix_size_bytes,
														 (void *) Y, &err));

		// Set the Kernel Arguments
		int narg = 0;
		OCL_CHECK(err, err = krnl.setArg(narg++, Uplo));
		OCL_CHECK(err, err = krnl.setArg(narg++, TransA));
		OCL_CHECK(err, err = krnl.setArg(narg++, Diag));
		OCL_CHECK(err, err = krnl.setArg(narg++, N));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sa));
		OCL_CHECK(err, err = krnl.setArg(narg++, lda));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sx));
		OCL_CHECK(err, err = krnl.setArg(narg++, incX));
		OCL_CHECK(err, err = krnl.setArg(narg++, Sy));

		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sx}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sa}, 0 /* 0 means from host*/));
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, 0 /* 0 means from host*/));
		std::cout<<"All sending to kenrel, its problem"<<std::endl;
		// Launch the Kernel
		OCL_CHECK(err, err = q.enqueueTask(krnl));
		// Copy Result from Device Global Memory to Host Local Memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({Sy}, CL_MIGRATE_MEM_OBJECT_HOST));

		clReleaseKernel(krnl.get());

		for(int i=0;i<N;i++){
			X[i]=Y[i];
		}
}


void OOPS_trsv(const char Uplo, const char TransA, const char Diag,
                 const int N, const float  *A, const int lda, float  *X, const int incX){
	float* A_trans = (float*) OOPS_malloc(sizeof(float)*N*N);
	for (int j = 0; j < N; j++){
		for (int i = 0; i < N; i++){
			A_trans[j*N+i] = A[i*N + j];
		}
	}

	cl_int err;

	// number of Compute Units to spawn
	int nstripe = 1;
	std::vector<cl::Kernel> krnls(nstripe);

	// This call will get the kernel object from program. A kernel is an OpenCL function that is executed on the FPGA.
	std::string krnl_name = "krnl_trsv";
	for (int i = 0; i < nstripe; i++) {
		std::string cu_id = std::to_string(i + 1);
		std::string krnl_name_full = krnl_name + ":{" + krnl_name +"_" + cu_id + "}";
		printf("Creating a kernel [%s] for CU(%d)\n", krnl_name_full.c_str(), i);
		OCL_CHECK(err, krnls[i] = cl::Kernel(program, krnl_name_full.c_str(), &err));
	}

	std::vector<int> stripe_ncols(nstripe);
	std::vector<int> stripe_col_offset(nstripe);
	std::vector<int> stripe_nterms_A(nstripe);
	std::vector<int> stripe_nterms_B(nstripe);
	std::vector<int> stripe_nterms_X(nstripe);
	
	int stripesize = 1; // B cannot be split in tiles, as it is 1D. just copy it from TRSM and specialize for 1 column
	int remainder  = 0;
	for (int i = 0; i < nstripe; i++) {
		// set-up stripe info
		stripe_nterms_A[i] = N * N; // since A is an upper triangular matrix

		if ( i <= remainder ) {
			stripe_ncols[i]   = stripesize + 1;
			if ( i == remainder )
				stripe_ncols[i]--;
		} 
		else {
			stripe_ncols[i]		= stripesize;
			if(i>0)
				stripe_col_offset[i] = stripe_col_offset[i-1] + stripe_ncols[i];
		}
		stripe_nterms_B[i]  = N * stripe_ncols[i];
		stripe_nterms_X[i]  = N * stripe_ncols[i];
		// printf("ROW BALANCING	 stripe = %d\tncols = %d\tpart_A = %d\tpart_B = %d\tpart_X = %d (start col offset = %d)\n", i, stripe_ncols[i], stripe_nterms_A[i], stripe_nterms_B[i], stripe_nterms_X[i], stripe_col_offset[i]);
	}

	/*******************************************************************************************/
	int W = 16;
	int values = (N*(N+1))/2; // total nonzero values of A triangular matrix
	int num_of_CUs = 16;
	int values_per_stream2 = values/num_of_CUs;
	int cnt = 0;
	int cnt2 = 0;
	int row_break[num_of_CUs-1];
	for (int i=1; i<=N; i++){
		cnt += i;
		if(cnt >= values_per_stream2)
		{
			cnt = 0;
			row_break[cnt2] = i;
			// printf("cnt2 = %d, split in line %d (before)\n", cnt2, row_break[cnt2]);
			row_break[cnt2] = row_break[cnt2]/W;
			row_break[cnt2] = row_break[cnt2]*W;
			// printf("cnt2 = %d, split in line %d (after)\n", cnt2, row_break[cnt2]);
			cnt2++;
		}
	}
	// for(int i=0; i<num_of_CUs-1; i++)
	// 	printf("finally, row_break[%d] = %d\n", i, row_break[i]);
	/*******************************************************************************************/

	std::vector<cl::Buffer> buffer_A1(nstripe);
	std::vector<cl::Buffer> buffer_A2(nstripe);
	std::vector<cl::Buffer> buffer_A3(nstripe);
	std::vector<cl::Buffer> buffer_A4(nstripe);
	std::vector<cl::Buffer> buffer_A5(nstripe);
	std::vector<cl::Buffer> buffer_A6(nstripe);
	std::vector<cl::Buffer> buffer_A7(nstripe);
	std::vector<cl::Buffer> buffer_A8(nstripe);
	std::vector<cl::Buffer> buffer_A9(nstripe);
	std::vector<cl::Buffer> buffer_A10(nstripe);
	std::vector<cl::Buffer> buffer_A11(nstripe);
	std::vector<cl::Buffer> buffer_A12(nstripe);
	std::vector<cl::Buffer> buffer_A13(nstripe);
	std::vector<cl::Buffer> buffer_A14(nstripe);
	std::vector<cl::Buffer> buffer_A15(nstripe);
	std::vector<cl::Buffer> buffer_A16(nstripe);
	std::vector<cl::Buffer> buffer_B(nstripe);
	std::vector<cl::Buffer> buffer_X(nstripe);

	float* _A[nstripe];
	float* _B[nstripe];
	float* _X[nstripe];
	
	// Allocate Buffer in Global Memory
	for (int i = 0; i < nstripe; i++) {
		_A[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_A[i]*sizeof(float)));
		memcpy(_A[i],A_m_trans,stripe_nterms_A[i]*sizeof(float));
		
		_B[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_B[i]*sizeof(float)));
		for(int ii=0; ii<N; ii++)
			for(int jj=0; jj<stripe_ncols[i]; jj++)
				_B[i][ii*stripe_ncols[i] + jj] = B[ii*1 + jj + stripe_col_offset[i]];

		_X[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_X[i]*sizeof(float)));


		OCL_CHECK(err, buffer_A1[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A2[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A3[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A4[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A5[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A6[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A7[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A8[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A9[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A10[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A11[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A12[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A13[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A14[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A15[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A16[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_B[i]   = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,   stripe_nterms_B[i]*sizeof(float), _B[i],  &err));
		OCL_CHECK(err, buffer_X[i]   = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY,	stripe_nterms_X[i]*sizeof(float), _X[i], &err));
	}

	// Set the Kernel Arguments
	for (int i = 0; i < nstripe; i++) {
		int narg=0;

		OCL_CHECK(krnls[i].setArg(narg++,Uplo));
		OCL_CHECK(krnls[i].setArg(narg++,TransA));
		OCL_CHECK(krnls[i].setArg(narg++,Diag));

		OCL_CHECK(krnls[i].setArg(narg++,K));

		OCL_CHECK(krnls[i].setArg(narg++,buffer_A1[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A2[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A3[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A4[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A5[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A6[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A7[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A8[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A9[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A10[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A11[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A12[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A13[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A14[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A15[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_A16[i]));
		OCL_CHECK(krnls[i].setArg(narg++,lda));

		OCL_CHECK(krnls[i].setArg(narg++,buffer_B[i]));
		OCL_CHECK(krnls[i].setArg(narg++,buffer_X[i]));
		OCL_CHECK(krnls[i].setArg(narg++,ldb));

		for(int k=0; k<num_of_CUs-1; k++)
			OCL_CHECK(krnls[i].setArg(narg++,row_break[k]));

		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_A1[i],buffer_A2[i],buffer_A3[i],buffer_A4[i],buffer_A5[i],buffer_A6[i],buffer_A7[i],buffer_A8[i],buffer_A9[i],buffer_A10[i],buffer_A11[i],buffer_A12[i],buffer_A13[i],buffer_A14[i],buffer_A15[i],buffer_A16[i],buffer_B[i],buffer_X[i]}, 0));

	}
	q.finish();
	
	// Launch the Kernel
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueTask(krnls[i]));
	}
	q.finish();

	// Copy Result from Device Global Memory to Host Local Memory
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_X[i]},1));
		for(int ii=0; ii<K; ii++){
			for(int jj=0; jj<stripe_ncols[i]; jj++){
				X[ii*1 + jj + stripe_col_offset[i]] = _X[i][ii*stripe_ncols[i] + jj];
			}
		}
	}
	q.finish();

	for (int i = 0; i < nstripe; i++){
		clReleaseKernel(krnls[i].get());
	}
    free(A_trans);
}

void OOPS_gemm(const char TransA, const char TransB, const int M, const int N, const int K, const float alpha, const float  *A, const int lda,
				const float  *B, const int ldb, const float beta, float  *C, const int ldc)
{
	cl_int err;

	// number of Compute Units to spawn
	int nstripe = 8;
	std::vector<cl::Kernel> krnls(nstripe);

	// This call will get the kernel object from program. A kernel is an OpenCL function that is executed on the FPGA.
	std::string krnl_name = "krnl_gemm";
	for (int i = 0; i < nstripe; i++) {
		std::string cu_id = std::to_string(i + 1);
		std::string krnl_name_full = krnl_name + ":{" + krnl_name +"_" + cu_id + "}";
		printf("Creating a kernel [%s] for CU(%d)\n", krnl_name_full.c_str(), i);
		OCL_CHECK(err, krnls[i] = cl::Kernel(program, krnl_name_full.c_str(), &err));
	}

	std::vector<int> stripe_nrows(nstripe);
	std::vector<int> stripe_start_A(nstripe);
	std::vector<int> stripe_start_C(nstripe);
	std::vector<int> stripe_nterms_A(nstripe);
	std::vector<int> stripe_nterms_B(nstripe);
	std::vector<int> stripe_nterms_C(nstripe);

	int stripesize = M/nstripe;
	int remainder  = M%nstripe;
	for (int i = 0; i < nstripe; i++) {
		// set-up stripe info
		stripe_nterms_B[i] = K * N;
		if ( i <= remainder ) {
			stripe_nrows[i]   = stripesize + 1;
			if ( i == remainder )
				stripe_nrows[i]--;
			stripe_nterms_A[i]  = stripe_nrows[i] * K;
			stripe_nterms_C[i]  = stripe_nrows[i] * N;
			if(i>0){
				stripe_start_A[i]	= stripe_start_A[i-1] + stripe_nterms_A[i-1];
				stripe_start_C[i]	= stripe_start_C[i-1] + stripe_nterms_C[i-1];
			}
		} else {
			stripe_nrows[i]		= stripesize;
			stripe_nterms_A[i]  = stripe_nrows[i] * K;
			stripe_nterms_C[i]  = stripe_nrows[i] * N;
			if(i>0){
				stripe_start_A[i]	= stripe_start_A[i-1] + stripe_nterms_A[i-1];
				stripe_start_C[i]	= stripe_start_C[i-1] + stripe_nterms_C[i-1];
			}
		}
		// printf("ROW BALANCING	 stripe = %d\tnrows = %d\tpart_A = %d (start = %d)\tpart_B = %d\tpart_C = %d (start = %d)\n", i, stripe_nrows[i], stripe_nterms_A[i], stripe_start_A[i], stripe_nterms_B[i], stripe_nterms_C[i], stripe_start_C[i]);
	}

	std::vector<cl::Buffer> buffer_A(nstripe);
	std::vector<cl::Buffer> buffer_B(nstripe);
	std::vector<cl::Buffer> buffer_C_in(nstripe);
	std::vector<cl::Buffer> buffer_C_out(nstripe);

	float* _A[nstripe];
	float* _B[nstripe];
	float* _C_in[nstripe];
	float* _C_out[nstripe];
	
	// Allocate Buffer in Global Memory
	for (int i = 0; i < nstripe; i++) {
		_A[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_A[i]*sizeof(float)));
		memcpy(_A[i],A+stripe_start_A[i],stripe_nterms_A[i]*sizeof(float));
		// printf("buffer_A[%d] %lf MB\n", i, stripe_nterms_A[i]*sizeof(float)/(1024*1024.0));

		_B[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_B[i]*sizeof(float)));
		memcpy(_B[i],B,stripe_nterms_B[i]*sizeof(float));
		// printf("buffer_B[%d] %lf MB\n", i, stripe_nterms_B[i]*sizeof(float)/(1024*1024.0));

		_C_in[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_C[i]*sizeof(float)));
		memcpy(_C_in[i],C+stripe_start_C[i],stripe_nterms_C[i]*sizeof(float));
		// printf("buffer_C_in[%d] %lf MB\n", i, stripe_nterms_C[i]*sizeof(float)/(1024*1024.0));

		_C_out[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_C[i]*sizeof(float)));
		// printf("buffer_C_out[%d] %lf MB\n\n", i, stripe_nterms_C[i]*sizeof(float)/(1024*1024.0));

		OCL_CHECK(err, buffer_A[i]     = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  stripe_nterms_A[i]*sizeof(float), _A[i],     &err));
		OCL_CHECK(err, buffer_B[i]     = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  stripe_nterms_B[i]*sizeof(float), _B[i],     &err));
		OCL_CHECK(err, buffer_C_in[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  stripe_nterms_C[i]*sizeof(float), _C_in[i],  &err));
		OCL_CHECK(err, buffer_C_out[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, stripe_nterms_C[i]*sizeof(float), _C_out[i], &err));
	}

	// Set the Kernel Arguments
	for (int i = 0; i < nstripe; i++) {
		int narg=0;
		OCL_CHECK(err, err = krnls[i].setArg(narg++,Layout));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,TransA));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,TransB));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,stripe_nrows[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,N));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,K));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,alpha));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_A[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,lda));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_B[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,ldb));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,beta));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_C_in[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_C_out[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,ldc));

		// https://xilinx.github.io/Vitis_Accel_Examples/2019.2/html/data_transfer.html
		// After creating buffer using Host Mem Pointer, clEnqueueMigrateMemObjects can be used for immediate migration
		// of data without considering the fact that data is actually needed or not by kernel operation.
		// Copy input data to device global memory
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_A[i],buffer_B[i],buffer_C_in[i]}, 0 /* 0 means from host */));
	}
	q.finish();
	
	// Launch the Kernel
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueTask(krnls[i]));
	}
	q.finish();

	// Copy Result from Device Global Memory to Host Local Memory
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_C_out[i]},1));
		memcpy(C_out+stripe_start_C[i],_C_out[i], stripe_nrows[i]*N*sizeof(float));
	}
	q.finish();

	for (int i = 0; i < nstripe; i++){
		clReleaseKernel(krnls[i].get());
	}
}


void OOPS_trmm(const char Side, const char Uplo, const  char TransA, const char Diag, const int M, const int N, const float alpha,
				const float *A, const int lda, const float *B, const int ldb, float *C)
{
	float* A_triang= (float *)OOPS_malloc(sizeof(float)*M*(M+1)/2);
	int cnt=0;
	for (int i = 0; i < M; i++){
		for (int j = 0; j < M; j++){
			if(i<=j)
				A_triang[cnt++] = A[i * M + j];
		}
	}

	cl_int err;

	// number of Compute Units to spawn
	int nstripe = 4;
	std::vector<cl::Kernel> krnls(nstripe);

	// This call will get the kernel object from program. A kernel is an OpenCL function that is executed on the FPGA.
	std::string krnl_name = "krnl_trmm";
	for (int i = 0; i < nstripe; i++) {
		std::string cu_id = std::to_string(i + 1);
		std::string krnl_name_full = krnl_name + ":{" + krnl_name +"_" + cu_id + "}";
		printf("Creating a kernel [%s] for CU(%d)\n", krnl_name_full.c_str(), i);
		OCL_CHECK(err, krnls[i] = cl::Kernel(program, krnl_name_full.c_str(), &err));
	}

	std::vector<int> stripe_ncols(nstripe);
	std::vector<int> stripe_col_offset(nstripe);
	std::vector<int> stripe_nterms_A(nstripe);
	std::vector<int> stripe_nterms_B(nstripe);
	std::vector<int> stripe_nterms_C(nstripe);

	int stripesize = N/nstripe; // now B will be split in stripes, so divide by columns (B has N columns)
	int remainder  = N%nstripe;
	for (int i = 0; i < nstripe; i++) {
		// set-up stripe info
		stripe_nterms_A[i] = M*(M+1)/2; // since A is an upper triangular matrix
		if ( i <= remainder ) {
			stripe_ncols[i]   = stripesize + 1;
			if ( i == remainder )
				stripe_ncols[i]--;
		}
		else {
			stripe_ncols[i]		= stripesize;
			if(i>0)
				stripe_col_offset[i] = stripe_col_offset[i-1] + stripe_ncols[i];
		}
		stripe_nterms_B[i]  = M * stripe_ncols[i];
		stripe_nterms_C[i]  = M * stripe_ncols[i];
		// printf("ROW BALANCING	 stripe = %d\tncols = %d\tpart_A = %d\tpart_B = %d\tpart_C = %d (start col offset = %d)\n", i, stripe_ncols[i], stripe_nterms_A[i], stripe_nterms_B[i], stripe_nterms_C[i], stripe_col_offset[i]);
	}

	std::vector<cl::Buffer> buffer_A(nstripe);
	std::vector<cl::Buffer> buffer_B(nstripe);
	std::vector<cl::Buffer> buffer_C(nstripe);

	float* _A[nstripe];
	float* _B[nstripe];
	float* _C[nstripe];
	
	// Allocate Buffer in Global Memory
	for (int i = 0; i < nstripe; i++) {
		_A[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_A[i]*sizeof(float)));
		memcpy(_A[i],A_triang,stripe_nterms_A[i]*sizeof(float));
		// printf("buffer_A[%d] %lf MB\n", i, stripe_nterms_A[i]*sizeof(float)/(1024*1024.0));

		_B[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_B[i]*sizeof(float)));
		for(int ii=0; ii<M; ii++)
			for(int jj=0; jj<stripe_ncols[i]; jj++)
				_B[i][ii*stripe_ncols[i] + jj] = B[ii*N + jj + stripe_col_offset[i]];
		// printf("buffer_B[%d] %lf MB\n", i, stripe_nterms_B[i]*sizeof(float)/(1024*1024.0));

		_C[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_C[i]*sizeof(float)));
		// printf("buffer_C[%d] %lf MB\n\n", i, stripe_nterms_C[i]*sizeof(float)/(1024*1024.0));

		OCL_CHECK(err, buffer_A[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_B[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  stripe_nterms_B[i]*sizeof(float), _B[i], &err));
		OCL_CHECK(err, buffer_C[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, stripe_nterms_C[i]*sizeof(float), _C[i], &err));
	}

	// Set the Kernel Arguments
	for (int i = 0; i < nstripe; i++) {
		int narg=0;
		OCL_CHECK(err, err = krnls[i].setArg(narg++,Side));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,Uplo));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,TransA));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,Diag));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,M));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,stripe_ncols[i]));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,alpha));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_A[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,lda));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_B[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_C[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,ldb));

		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_A[i],buffer_B[i]}, 0));
	}
	q.finish();
	
	// Launch the Kernel
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueTask(krnls[i]));
	}
	q.finish();

	// Copy Result from Device Global Memory to Host Local Memory
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_C[i]},1));
		for(int ii=0; ii<K; ii++){
			for(int jj=0; jj<stripe_ncols[i]; jj++){
				C[ii*N + jj + stripe_col_offset[i]] = _C[i][ii*stripe_ncols[i] + jj];
			}
		}
	}
	q.finish();

	for (int i = 0; i < nstripe; i++){
		clReleaseKernel(krnls[i].get());
	}
	free(A_triang);
}

void OOPS_symm(const char Side, const char Uplo, const int M, const int N, const float alpha, const float  *A, const int lda, const float  *B,
		const int ldb, const float beta, float  *C, const int ldc)
{
	float* A_triang= (float *)OOPS_malloc(sizeof(float)*M*(M+1)/2);
	int cnt=0;
	for (int i = 0; i < M; i++){
		for (int j = 0; j < M; j++){
			if(i<=j)
				A_triang[cnt++] = A[i * M + j];
		}
	}

	cl_int err;

	// number of Compute Units to spawn
	int nstripe = 2;
	std::vector<cl::Kernel> krnls(nstripe);

	// This call will get the kernel object from program. A kernel is an OpenCL function that is executed on the FPGA.
	std::string krnl_name = "krnl_symm";
	for (int i = 0; i < nstripe; i++) {
		std::string cu_id = std::to_string(i + 1);
		std::string krnl_name_full = krnl_name + ":{" + krnl_name +"_" + cu_id + "}";
		printf("Creating a kernel [%s] for CU(%d)\n", krnl_name_full.c_str(), i);
		OCL_CHECK(err, krnls[i] = cl::Kernel(program, krnl_name_full.c_str(), &err));
	}

	std::vector<int> stripe_ncols(nstripe);
	std::vector<int> stripe_col_offset(nstripe);
	std::vector<int> stripe_nterms_A(nstripe);
	std::vector<int> stripe_nterms_B(nstripe);
	std::vector<int> stripe_nterms_C_in(nstripe);
	std::vector<int> stripe_nterms_C_out(nstripe);

	int stripesize = N/nstripe; // now B will be split in stripes, so divide by columns (B has N columns)
	int remainder  = N%nstripe;
	for (int i = 0; i < nstripe; i++) {
		// set-up stripe info
		stripe_nterms_A[i] = M*(M+1)/2; // since A is an upper triangular matrix
		if ( i <= remainder ) {
			stripe_ncols[i]   = stripesize + 1;
			if ( i == remainder )
				stripe_ncols[i]--;
		}
		else {
			stripe_ncols[i]		= stripesize;
			if(i>0)
				stripe_col_offset[i] = stripe_col_offset[i-1] + stripe_ncols[i];
		}
		stripe_nterms_B[i]  = M * stripe_ncols[i];
		stripe_nterms_C_in[i]  = M * stripe_ncols[i];
		stripe_nterms_C_out[i]  = M * stripe_ncols[i];
		// printf("ROW BALANCING	 stripe = %d\tncols = %d\tpart_A = %d\tpart_B = %d\tpart_C_in = %d\tpart_C_in = %d (start col offset = %d)\n", i, stripe_ncols[i], stripe_nterms_A[i], stripe_nterms_B[i], stripe_nterms_C_in[i], stripe_nterms_C_out[i], stripe_col_offset[i]);
	}

	std::vector<cl::Buffer> buffer_A(nstripe);
	std::vector<cl::Buffer> buffer_B(nstripe);
	std::vector<cl::Buffer> buffer_C_in(nstripe);
	std::vector<cl::Buffer> buffer_C_out(nstripe);

	float* _A[nstripe];
	float* _B[nstripe];
	float* _C_in[nstripe];
	float* _C_out[nstripe];
	
	// Allocate Buffer in Global Memory
	for (int i = 0; i < nstripe; i++) {
		_A[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_A[i]*sizeof(float)));
		memcpy(_A[i],A_triang,stripe_nterms_A[i]*sizeof(float));
		// printf("buffer_A[%d] %lf MB\n", i, stripe_nterms_A[i]*sizeof(float)/(1024*1024.0));

		_B[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_B[i]*sizeof(float)));
		for(int ii=0; ii<M; ii++)
			for(int jj=0; jj<stripe_ncols[i]; jj++)
				_B[i][ii*stripe_ncols[i] + jj] = B[ii*N + jj + stripe_col_offset[i]];
		// printf("buffer_B[%d] %lf MB\n", i, stripe_nterms_B[i]*sizeof(float)/(1024*1024.0));

		_C_in[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_C_in[i]*sizeof(float)));
		for(int ii=0; ii<M; ii++)
			for(int jj=0; jj<stripe_ncols[i]; jj++)
				_C_in[i][ii*stripe_ncols[i] + jj] = C[ii * N + jj + stripe_col_offset[i]];
		// printf("buffer_C_in[%d] %lf MB\n", i, stripe_nterms_C_in[i]*sizeof(float)/(1024*1024.0));

		_C_out[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_C_out[i]*sizeof(float)));
		// printf("buffer_C_out[%d] %lf MB\n\n", i, stripe_nterms_C_out[i]*sizeof(float)/(1024*1024.0));

		OCL_CHECK(err, buffer_A[i]     = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  stripe_nterms_A[i]*sizeof(float),     _A[i],     &err));
		OCL_CHECK(err, buffer_B[i]     = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  stripe_nterms_B[i]*sizeof(float),     _B[i],     &err));
		OCL_CHECK(err, buffer_C_in[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  stripe_nterms_C_in[i]*sizeof(float),  _C_in[i],  &err));
		OCL_CHECK(err, buffer_C_out[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, stripe_nterms_C_out[i]*sizeof(float), _C_out[i], &err));
	}

	// Set the Kernel Arguments
	for (int i = 0; i < nstripe; i++) {
		int narg=0;

		OCL_CHECK(err, err = krnls[i].setArg(narg++,Side));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,Uplo));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,M));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,stripe_ncols[i]));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,alpha));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_A[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,lda));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_B[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,ldb));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,beta));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_C_in[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_C_out[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,ldc));

		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_A[i],buffer_B[i],buffer_C_in[i]}, 0));
	}
	q.finish();
	
	// Launch the Kernel
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueTask(krnls[i]));
	}
	q.finish();

	// Copy Result from Device Global Memory to Host Local Memory
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_C_out[i]},1));
		for(int ii=0; ii<M; ii++){
			for(int jj=0; jj<stripe_ncols[i]; jj++){
				C[ii*M + jj + stripe_col_offset[i]] = _C_out[i][ii*stripe_ncols[i] + jj];
			}
		}
	}
	q.finish();

	for (int i = 0; i < nstripe; i++){
		clReleaseKernel(krnls[i].get());
	}
	free(A_triang);
}

void OOPS_trsm(const char Side, const char Uplo, const  char TransA, const char Diag, const int M, const int N, const float alpha,
		const float *A, const int lda, const float *B, const int ldb, float *C)
{
	float* A_trans = (float*) OOPS_malloc(sizeof(float)*M*M);
	for (int j = 0; j < M; j++){
		for (int i = 0; i < M; i++){
			A_trans[j*M+i] = A[i*M + j];
		}
	}

	cl_int err;

	// number of Compute Units to spawn
	int nstripe = 10;
	std::vector<cl::Kernel> krnls(nstripe);

	// This call will get the kernel object from program. A kernel is an OpenCL function that is executed on the FPGA.
	std::string krnl_name = "krnl_trsm";
	for (int i = 0; i < nstripe; i++) {
		std::string cu_id = std::to_string(i + 1);
		std::string krnl_name_full = krnl_name + ":{" + krnl_name +"_" + cu_id + "}";
		printf("Creating a kernel [%s] for CU(%d)\n", krnl_name_full.c_str(), i);
		OCL_CHECK(err, krnls[i] = cl::Kernel(program, krnl_name_full.c_str(), &err));
	}

	std::vector<int> stripe_ncols(nstripe);
	std::vector<int> stripe_col_offset(nstripe);
	std::vector<int> stripe_nterms_A(nstripe);
	std::vector<int> stripe_nterms_B(nstripe);
	std::vector<int> stripe_nterms_C(nstripe);

	int stripesize = N/nstripe; // now B will be split in stripes, so divide by columns (B has N columns)
	int remainder  = N%nstripe;
	for (int i = 0; i < nstripe; i++) {
		// set-up stripe info
		stripe_nterms_A[i] = M * M;

		if ( i <= remainder ) {
			stripe_ncols[i]   = stripesize + 1;
			if ( i == remainder )
				stripe_ncols[i]--;
		}
		else {
			stripe_ncols[i]		= stripesize;
			if(i>0)
				stripe_col_offset[i] = stripe_col_offset[i-1] + stripe_ncols[i];
		}
		stripe_nterms_B[i]  = M * stripe_ncols[i];
		stripe_nterms_C[i]  = M * stripe_ncols[i];
		// printf("ROW BALANCING	 stripe = %d\tncols = %d\tpart_A = %d\tpart_B = %d\tpart_C = %d (start col offset = %d)\n", i, stripe_ncols[i], stripe_nterms_A[i], stripe_nterms_B[i], stripe_nterms_C[i], stripe_col_offset[i]);
	}

	/*******************************************************************************************/
	int W = 16;
	int values = (M*(M+1))/2; // total nonzero values of A triangular matrix
	int num_of_CUs = 2;
	int values_per_stream2 = values/num_of_CUs;
	int cnt = 0;
	int cnt2 = 0;
	int row_break[num_of_CUs-1];
	for (int i=1; i<=M; i++){
		cnt += i;
		if(cnt >= values_per_stream2)
		{
			cnt = 0;
			row_break[cnt2] = i;
			// printf("cnt2 = %d, split in line %d (before)\n", cnt2, row_break[cnt2]);
			row_break[cnt2] = row_break[cnt2]/W;
			row_break[cnt2] = row_break[cnt2]*W;
			// printf("cnt2 = %d, split in line %d (after)\n", cnt2, row_break[cnt2]);
			cnt2++;
		}
	}
	// for(int i=0; i<num_of_CUs-1; i++)
	// 	printf("finally, row_break[%d] = %d\n", i, row_break[i]);
	/*******************************************************************************************/


	std::vector<cl::Buffer> buffer_A1(nstripe);
	std::vector<cl::Buffer> buffer_A2(nstripe);
	std::vector<cl::Buffer> buffer_B(nstripe);
	std::vector<cl::Buffer> buffer_C(nstripe);

	float* _A[nstripe];
	float* _B[nstripe];
	float* _C[nstripe];
	
	// Allocate Buffer in Global Memory
	for (int i = 0; i < nstripe; i++) {
		_A[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_A[i]*sizeof(float)));
		memcpy(_A[i],A_trans,stripe_nterms_A[i]*sizeof(float));
		// printf("buffer_A1[%d] %lf MB\n", i, stripe_nterms_A[i]*sizeof(float)/(1024*1024.0));
		
		_B[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_B[i]*sizeof(float)));
		for(int jj=0; jj<stripe_ncols[i]; jj++)
			for(int ii=0; ii<M; ii++)
				_B[i][ii*stripe_ncols[i] + jj] = B[ii*N + jj + stripe_col_offset[i]];
		// printf("buffer_B[%d] %lf MB\n", i, stripe_nterms_B[i]*sizeof(float)/(1024*1024.0));

		_C[i]= (float*) OOPS_malloc((size_t)(stripe_nterms_C[i]*sizeof(float)));
		// printf("buffer_C[%d] %lf MB\n\n", i, stripe_nterms_C[i]*sizeof(float)/(1024*1024.0));
	
		OCL_CHECK(err, buffer_A1[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,   stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_A2[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,   stripe_nterms_A[i]*sizeof(float), _A[i], &err));
		OCL_CHECK(err, buffer_B[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,   stripe_nterms_B[i]*sizeof(float), _B[i],  &err));
		OCL_CHECK(err, buffer_C[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY,	stripe_nterms_C[i]*sizeof(float), _C[i],  &err));
	}

	// Set the Kernel Arguments
	for (int i = 0; i < nstripe; i++) {
		int narg=0;

		OCL_CHECK(err, err = krnls[i].setArg(narg++,Side));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,Uplo));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,TransA));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,Diag));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,M));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,stripe_ncols[i]));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,alpha));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_A1[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_A2[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,lda));

		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_B[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_C[i]));
		OCL_CHECK(err, err = krnls[i].setArg(narg++,ldb));

		for(int j=0; j<num_of_CUs; j++)
			OCL_CHECK(err, err = krnls[i].setArg(narg++,row_break[j])); // it will only be 1 here...anyway

		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_A1[i],buffer_A2[i],buffer_B[i],buffer_C}, 0));
	}
	q.finish();
	
	// Launch the Kernel
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueTask(krnls[i]));
	}
	q.finish();

	// Copy Result from Device Global Memory to Host Local Memory
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_C_out[i]},1));
		for(int ii=0; ii<M; ii++){
			for(int jj=0; jj<stripe_ncols[i]; jj++){
				C[ii*N + jj + stripe_col_offset[i]] = _C[i][ii*stripe_ncols[i] + jj];
			}
		}
	}
	q.finish();

	for (int i = 0; i < nstripe; i++){
		clReleaseKernel(krnls[i].get());
	}
	free(A_trans);
}


/****************************************************/
int bin_search(const int ii, int iend, const int *const v)
{
	int istart = 0;
	int pos = iend/2;
	while (iend-istart > 1){
		if (v[pos] > ii){
			// Get the left interval
			iend = pos;
		} else {
			// Get the right interval
			istart = pos;
		}
		pos = (istart+iend)/2;
	}
	return pos;
}

#undef SWAP // it was defined already in lib/macros/macrolib.h
template <typename TYPE>
inline void SWAP(TYPE &i1, TYPE &i2){TYPE tmp = i1; i1 = i2; i2 = tmp;}
template void SWAP<int>(int &, int & );
template void SWAP<float>(float &, float & );

void heapsort_2v(int* __restrict__ x1, float* __restrict__ x2, const int n)
{
	for (int node = 1; node < n; node ++){
		int i = node;
		int j = i/2;
		while( x1[j] < x1[i] ){
			SWAP(x1[j],x1[i]);
			SWAP(x2[j],x2[i]);
			i = j;
			j = i/2;
			if (i == 0) break;
		}
	}
	
	for (int i = n-1; i > 0; i --){
		SWAP(x1[i],x1[0]);
		SWAP(x2[i],x2[0]);
		int k = i - 1;
		int ik = 0; 
		int jk = 1;
		if (k >= 2){
			if (x1[2] > x1[1]) jk = 2;
		}
		bool cont_cycle = false;
		if (jk <= k){
			if (x1[jk] > x1[ik]) cont_cycle = true;
		}
		while (cont_cycle){
			SWAP(x1[jk],x1[ik]);
			SWAP(x2[jk],x2[ik]);
			ik = jk;
			jk = ik*2;
			if (jk+1 <= k){
				if (x1[jk+1] > x1[jk]) jk = jk+1;
			}
			cont_cycle = false;
			if (jk <= k){
				if (x1[jk] > x1[ik]) cont_cycle = true;
			}
		}
	}
}
/****************************************************/

void OOPS_SpMV(const int nrows, const int nterm,
               const long int* iat, const int* ja, const float* __restrict__ coef,
               const float* __restrict__ x, float* __restrict__ b)
{
	// number of Compute Units to spawn
	int nstripe = 16;
	std::vector<cl::Kernel> krnls(nstripe);

	// This call will get the kernel object from program. A kernel is an OpenCL function that is executed on the FPGA.
	std::string krnl_name = "krnl_SpMV";
	for (int i = 0; i < nstripe; i++) {
		std::string cu_id = std::to_string(i + 1);
		std::string krnl_name_full = krnl_name + ":{" + krnl_name +"_" + cu_id + "}";
		printf("Creating a kernel [%s] for CU(%d)\n", krnl_name_full.c_str(), i);
		OCL_CHECK(err, krnls[i] = cl::Kernel(program, krnl_name_full.c_str(), &err));
	}

	// allocate padded buffers
	int padding_factor = 64 / sizeof(float); // 64 is 512-bits, which is the width of the HBM interface
	// printf("padding_factor = %d\n", padding_factor);
	nterm_padded = nterm + nrows * padding_factor;
	long int * iat_padded  = (long int*) OOPS_malloc((size_t)((nrows+1) * sizeof(long int)));
	int * ja_padded   = (ColType*) OOPS_malloc((size_t)(nterm_padded * sizeof(ColType)));
	float * coef_padded = (float*) OOPS_malloc((size_t)(nterm_padded * sizeof(float)));

	/*******************************************************************************************/

	long int * irow_padded = (long int*) OOPS_malloc((size_t)(nterm_padded * sizeof(long int)));
	long int * ja_padded_int = (long int*) OOPS_malloc((size_t)( nterm_padded * sizeof(long int))); // this is needed for padding to be more fast

	// irow_padded initialization
	for (int i = 0; i < nterm_padded; i++)
		irow_padded[i] = -1;

	// allocate scratch buffers
	int *nt_add = (int*) OOPS_malloc((size_t)(m * sizeof(int)));

	// get number of new adding for each row
	for (int i = 0; i < nrows; i++) {
		int nt_row = iat[i+1]-iat[i];
		if(nt_row != 0){
			int resto  = nt_row % padding_factor;
			int mult   = int(nt_row / padding_factor);
			if ( resto == 0 ) {
				nt_add[i] = 0;
			} else {
				int nt_row_p  = (mult+1)*padding_factor;
				nt_add[i] = nt_row_p - nt_row;
				// printf("nt_add %d %d %d %d %d\n",i,nt_row,resto,mult,nt_add[i]);
			}
		}
		else
			nt_add[i] = padding_factor;
	}

	// copy entries
	for (int i = 0; i < nrows; i++) {
		int nt_row = iat[i+1]-iat[i];
		int jj = iat[i]+padding_factor*i;
		for (int j = 0; j < nt_row; j++) {
			irow_padded[jj+j] = i;
			ja_padded_int[jj+j] = ja[iat[i]+j];
			coef_padded[jj+j] = a[iat[i]+j];
		}
	}

	// right padding
	for (int i = 0; i < nrows/2; i++) {
		int nt_row = iat[i+1]-iat[i];

		int icol = i;
		for (int j = 0; j < nt_add[i]; j++) {
			bool find_icol = true;
			while(find_icol) {
				icol++;
				if ( icol > nrows-1 ) {
					printf("****ERROR PAD 1****\n");
					exit(-1);
				}
				// check if icol is already in the i-row
				int pos = bin_search(icol,nt_row,&(ja[iat[i]]));
				if (icol != ja[iat[i]+pos] ) find_icol = false;
			}
			// add zero term on icol-col of i-row
			int jj = iat[i]+padding_factor*i+nt_row+j;
			irow_padded[jj] = i;
			ja_padded_int[jj]   = icol;
			coef_padded[jj] = 0.;
		}

		// sorting
		if ( nt_add[i] > 0 ) {
			int jj = iat[i]+padding_factor*i;
			heapsort_2v(&(ja_padded_int[jj]),&(coef_padded[jj]),nt_row+nt_add[i]);
		}
	}

	// left padding
	for (int i = nrows-1; i >= nrows/2; i--) {
		int nt_row = iat[i+1]-iat[i];
		int icol = i;
		int icol_right=n-1;
		for (int j = 0; j < nt_add[i]; j++) {
			bool find_icol = true;
			while(find_icol) {
				icol--;
				// if ( icol > nrows-1 ) {
				if ( icol < 0 ) {
					// printf("****ERROR PAD 2****\n");
					// exit(-2);
					icol_right++;
					int pos = bin_search(icol_right,nt_row,&(ja[iat[i]]));
					if (icol_right != ja[iat[i]+pos] ) find_icol = false;
				}
				// check if icol is already in the i-row
				int pos = bin_search(icol,nt_row,&(ja[iat[i]]));
				if (icol != ja[iat[i]+pos] ) find_icol = false;
			}

			// add zero term on icol-col of i-row
			int jj = iat[i]+padding_factor*i+nt_row+j;
			irow_padded[jj] = i;
			if(icol<0){
				ja_padded_int[jj]   = icol_right;
			}
			else{
				ja_padded_int[jj]   = icol;
			}
			coef_padded[jj] = 0.;
		}
		// sorting
		if ( nt_add[i] > 0 ) {
			int jj = iat[i]+padding_factor*i;
			heapsort_2v(&(ja_padded_int[jj]),&(coef_padded[jj]),nt_row+nt_add[i]);
		}
	}

	// compaction
	long int ntermA_wp = 0;
	for (int i = 0; i < nrows; i++) {
		int nt_row = iat[i+1]-iat[i];
		int jj = iat[i]+padding_factor*i;
		for (int j = 0; j < nt_row+nt_add[i]; j++) {
			irow_padded[ntermA_wp+j] = irow_padded[jj+j];
			ja_padded_int[ntermA_wp+j]   = ja_padded_int[jj+j];
			coef_padded[ntermA_wp+j] = coef_padded[jj+j];
		}
		ntermA_wp += nt_row+nt_add[i];
	}
	free(nt_add);

	// Assembly iat for the Compressed Sparse Row (CSR) format
	// Note that the matrix is symmetric so CSR format == CSC format
	iat_padded[0] = 0;
	{
		int j = 0;
		for ( int i = 0; i < ntermA_wp; i++) {
		 ja_padded[i].range(0,30) = ja_padded_int[i];
		 if( irow_padded[i] > j ) {
				iat_padded[j+1] = i;
				ja_padded[i-1].range(31,31) = 1;
				j++;
			}
		}
		ja_padded[i].range(31,31) = 0;
	}
	iat_padded[m] = ntermA_wp;
	ja_padded[ntermA_wp-1].range(31,31) = 1;
	nterm_padded=ntermA_wp;
	
	free(irow_padded);
	free(ja_padded_int);

	/*******************************************************************************************/
	float* x_stream;
	RowType* iatbit;
	iatbit = (RowType*) OOPS_malloc((size_t)(nterm*sizeof(RowType)));
	x_stream = (float*) OOPS_malloc((size_t)(nterm*sizeof(float)));
	ColType col;
	for(int i = 0; i < nterm; i++) {
		col = ja_padded[i].range(0,30);
		iatbit[i] = ja_padded[i].range(31,31);
		x_stream[i] = x[col];
	}
	/*******************************************************************************************/

	std::vector<int> stripe_nrows(nstripe);
	std::vector<int> stripe_start_row(nstripe);
	std::vector<int> stripe_start_index(nstripe);
	std::vector<int> stripe_nterm(nstripe);

	// For each Compute Unit (nstripe), specify the number of elements each Compute Unit will be assigned with
	int stripesize = nrows/nstripe;
	int remainder  = nrows%nstripe;
	for (int i = 0; i < nstripe; i++) {
		// set-up stripe info
		if ( i <= remainder ) {
			stripe_nrows[i]        = stripesize + 1;
			stripe_start_row[i]    = i*stripe_nrows[i];
			stripe_start_index[i]  = iat_padded[stripe_start_row[i]];
			if ( i == remainder ) stripe_nrows[i]--;
		} else {
			stripe_nrows[i]        = stripesize;
			stripe_start_row[i]    = i*stripe_nrows[i] + remainder;
			stripe_start_index[i]  = iat_padded[stripe_start_row[i]];
		}
		stripe_nterm[i] = iat_padded[stripe_start_row[i]+stripe_nrows[i]] - stripe_start_index[i];
		printf("ROW_BAL: stripe = %d\tnrows = %d\tnterm = %d\n", i, stripe_nrows[i], stripe_nterm[i]);
	}
	double nterm_min, nterm_max, nterm_avg, nterm_std, nterm_skew;
	calculate_min_max_mean_std_skew(stripe_nterm, nstripe, &nterm_min, &nterm_max, &nterm_avg, &nterm_std, &nterm_skew);

	if(nterm_std/nterm_avg > 1000){
		printf("ROW_BAL: nterm_min = %lf, nterm_max = %lf, nterm_avg = %lf, nterm_std = %lf, nterm_skew = %lf\n", nterm_min, nterm_max, nterm_avg, nterm_std, nterm_skew);
		printf("Need to do NNZ_BAL and not ROW_BAL\n");
		int stripesize2 = nterm_padded/nstripe;
		// printf("NNZ_BAL NOW\tit should be %d nonzeros per CU\n", stripesize2);
		int curr_nterm = 0;
		int curr_nrow = 0;
		int stripe_id = 0;
		stripe_start_row[0] = 0;
		stripe_start_index[0] = 0;
		for(int i=0; i<m; i++){
			// if not in last stripe
			if(stripe_id < (nstripe-1))
			{
				curr_nrow++;
				curr_nterm += (iat_padded[i+1] - iat_padded[i]);
				if(curr_nterm > stripesize2){
					// means we have to cut-off the specific row and move to next stripe
					// next stripe will begin from next row
					// perhaps check one row forward (for better load balancing) - or cut off right before row changes (but need to go back? somehow)
					
					stripe_nrows[stripe_id] = curr_nrow;
					stripe_start_row[stripe_id+1] = i+1;
					stripe_start_index[stripe_id+1] = iat_padded[stripe_start_row[stripe_id+1]];
					// printf("stripe_id = %d\tcurr_nterm = %d\tstripe_nrows = %d\tstripe_start_row = %d\tstripe_start_index = %d\n", stripe_id, curr_nterm, stripe_nrows[stripe_id], stripe_start_row[stripe_id], stripe_start_index[stripe_id]);

					stripe_nterm[stripe_id] = iat_padded[stripe_start_row[stripe_id]+stripe_nrows[stripe_id]] - stripe_start_index[stripe_id];
					// printf("NNZ_BAL stripe = %d\tnrows = %d\tnterm = %d\n", stripe_id, stripe_nrows[stripe_id], stripe_nterm[stripe_id]);

					// reset and move on to next stripe
					curr_nrow = 0;
					curr_nterm = 0;
					stripe_id++;
				}
			}
			else{
				// we are in the last stripe
				// just get what is remaining
				// stripe_start_row will be ok
				// need to handle the rest of them
				stripe_nrows[stripe_id] = nrows - i;// + 1;
				stripe_nterm[stripe_id] = iat_padded[stripe_start_row[stripe_id]+stripe_nrows[stripe_id]] - stripe_start_index[stripe_id];
				printf("NNZ_BAL: stripe = %d\tnrows = %d\tnterm = %d\n", stripe_id, stripe_nrows[stripe_id], stripe_nterm[stripe_id]);
				break;
			}
		}
		calculate_min_max_mean_std_skew(stripe_nterm, nstripe, &nterm_min, &nterm_max, &nterm_avg, &nterm_std, &nterm_skew);
		printf("NNZ_BAL: nterm_min = %lf, nterm_max = %lf, nterm_avg = %lf, nterm_std = %lf, nterm_skew = %lf\n", nterm_min, nterm_max, nterm_avg, nterm_std, nterm_skew);
		// printf("time_rowbal vs time_nnzbal = %lf vs %lf\n", time_rowbal, time_nnzbal);
	}

	/*******************************************************************************************/

	std::vector<cl::Buffer> buffer_coef(nstripe);
	std::vector<cl::Buffer> buffer_iat(nstripe);
	std::vector<cl::Buffer> buffer_x(nstripe);
	std::vector<cl::Buffer> buffer_b(nstripe);

	float* _coef[nstripe];
	float* _b[nstripe];
	float* _x[nstripe];
	RowType* _iat[nstripe];

	for (int i = 0; i < nstripe; i++) {
		if(stripe_nterm[i]!=0){
			_coef[i]= (float*) OOPS_malloc((size_t)(stripe_nterm[i]*sizeof(float)));
			memcpy(_coef[i],coef_padded+stripe_start_index[i],stripe_nterm[i]*sizeof(float));
			// printf("buffer_coef[%d] %4.3lf MB\n", i, stripe_nterm[i]*sizeof(float)/(1024*1024.0));

			_iat[i]= (RowType*) OOPS_malloc((size_t)(stripe_nterm[i]*sizeof(RowType)));
			memcpy(_iat[i],iatbit+stripe_start_index[i],stripe_nterm[i]*sizeof(RowType));
			// printf("buffer_iat[%d] %lf MB\n", i, stripe_nterm[i]*sizeof(RowType)/(1024*1024.0));

			_x[i]= (float*) OOPS_malloc((size_t)(stripe_nterm[i]*sizeof(float)));
			memcpy(_x[i],x_stream+stripe_start_index[i],stripe_nterm[i]*sizeof(float));
			// printf("buffer_x[%d] %lf MB\n", i, stripe_nterm[i]*sizeof(float)/(1024*1024.0));

			_b[i]= (float*) OOPS_malloc((size_t)(stripe_nrows[i]*sizeof(float)));
			// printf("buffer_b[%d] %lf MB\n\n", i, stripe_nrows[i]*sizeof(float)/(1024*1024.0));

			OCL_CHECK(err, buffer_coef[i] = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  stripe_nterm[i]*sizeof(float),  _coef[i], &err));
			OCL_CHECK(err, buffer_iat[i]  = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  stripe_nterm[i]*sizeof(RowType), _iat[i], &err));
			OCL_CHECK(err, buffer_x[i]    = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  stripe_nterm[i]*sizeof(float),     _x[i], &err));
			OCL_CHECK(err, buffer_b[i]    = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, stripe_nrows[i]*sizeof(float),     _b[i], &err));
		}
	}

	// Set the Kernel Arguments
	for (int i = 0; i < nstripe; i++) {
		if(stripe_nterm[i]!=0){
			int narg=0;
			printf("Kernel %d nterm %d nrows %d\n", i, stripe_nterm[i], stripe_nrows[i]);
			OCL_CHECK(err, err = krnls[i].setArg(narg++,128)); // number of iterations
			OCL_CHECK(err, err = krnls[i].setArg(narg++,stripe_nrows[i]));
			OCL_CHECK(err, err = krnls[i].setArg(narg++,stripe_nterm[i]));
			OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_iat[i]));
			OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_coef[i]));
			OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_x[i]));
			OCL_CHECK(err, err = krnls[i].setArg(narg++,buffer_b[i]));

			OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_coef[i],buffer_ja[i],buffer_x[i]}, 0));
		}
	}
	q.finish();
	
	// Launch the Kernel
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueTask(krnls[i]));
	}
	q.finish();

	// Copy Result from Device Global Memory to Host Local Memory
	for (int i = 0; i < nstripe; i++){
		OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_b[i]},1));
		if(stripe_nterm[i]!=0){
			// printf("i=%d, stripe_nterm = %d\n", i, stripe_nterm[i]);
			memcpy(b+stripe_start_row[i],_b[i],stripe_nrows[i]*sizeof(float));
		}
	}
	q.finish();

	for (int i = 0; i < nstripe; i++){
		clReleaseKernel(krnls[i].get());
	}

	free(iat_padded);
	free(ja_padded);
	free(coef_padded);

	for(int i = 0; i < nstripe; i++){
		free(_coef[i]);
		free(_b[i]);
		free(_ja[i]);
	}
}