#include "oops.hpp"
#include "matrix_vector_generation.hpp"
#include <ctime>
#include <chrono>

using namespace std::chrono;


const int N=1600, incX=1, incY=1,M=20,KU=1,KL=1;
void main_copy(){
    float* X;
    float* Y;
    float* Y_sw;
    X=(float*)OOPS_malloc(sizeof(float)*N*incX);
    Y=(float*)OOPS_malloc(sizeof(float)*N*incY);
    Y_sw=(float*)OOPS_malloc(sizeof(float)*N*incY);

    vector_N(X,N,incX);

    high_resolution_clock::time_point t1 = high_resolution_clock::now();

	for(int i=0;i<N;i++){
		Y_sw[i] = X[i];
	}

    high_resolution_clock::time_point t2 = high_resolution_clock::now();

    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

    std::cout << "It took me " << time_span.count() << " seconds.";
    std::cout << std::endl;

	OOPS_copy( N, X, incX, Y, incY);

	int match = 0;
   	for (int i = 0; i < N; i++) {
   		if (abs(Y_sw[i]-Y[i])>0.001) {
   			std::cout << "Error: Result mismatch" << std::endl;
   	        std::cout << "i = " << i << " CPU result = " << Y_sw[i]
   	                      << " Device result = " << Y[i] << std::endl;
   	        match = 1;
   	        break;
   	    }
   	}

   	std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;

   	free(X);
   	free(Y);
   	free(Y_sw);

}


void main_nrm2(){
    float* X;
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    float result,sw_result=0;
    vector_N(X,N,incX);
	result = OOPS_nrm2( N, X, incX);

	for(int i=0;i<N;i++){
		sw_result+=X[i]*X[i];
	}

	sw_result=sqrt(sw_result);

	if(abs(sw_result-result)>0.01){
	    std::cout << "Error: Result mismatch" << std::endl;
	    std::cout << "SW " << sw_result << " HW " << result << std::endl;

	}
	else{
		std::cout << "TEST PASSED" << std::endl;
	}

	free(X);
}

void main_ddot(){
    float* X;
    float* Y;
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    Y= (float *)OOPS_malloc(sizeof(float)*N*incY);
    vector_N(X,N,incX);
    vector_N(Y,N,incY);
    float alpha=((float) rand()) / (float) RAND_MAX;

    double result,sw_result=alpha;

    for(int i=0;i<N;i++){
    	sw_result+=(X[i]*Y[i]);
    }

    result=OOPS_ddot( N, alpha, X,  incX, Y, incY);

    if(abs(sw_result-result)>0.01){
       	std::cout << "Error: Result mismatch" << std::endl;
        std::cout << "SW " << sw_result << " HW " << result << std::endl;

    }
    else{
        std::cout << "TEST PASSED" << std::endl;
    }
    free(X);
    free(Y);
}

void main_dot(){
    float* X;
    float* Y;
    float result,sw_result=0;
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    Y= (float *)OOPS_malloc(sizeof(float)*N*incY);
    vector_N(X,N,incX);
    vector_N(Y,N,incY);

	for(int i=0;i<N;i++){
		sw_result+=(X[i]*Y[i]);
	}
    result=OOPS_dot( N, X,  incX, Y, incY);

    if(abs(sw_result-result)>0.01){
           	std::cout << "Error: Result mismatch" << std::endl;
           	std::cout << "SW " << sw_result << " HW " << result << std::endl;

    }
    else{
        	std::cout << "TEST PASSED" << std::endl;
    }

	free(X);
	free(Y);
}

void main_rot(){
    float* X;
    float* sw_X;
    float* Y;
    float* sw_Y;
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    Y= (float *)OOPS_malloc(sizeof(float)*N*incY);
    sw_X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    sw_Y= (float *)OOPS_malloc(sizeof(float)*N*incY);

    vector_N(X,N,incX);
    vector_N(Y,N,incY);

    float c=((float) rand()) / (float) RAND_MAX,s=((float) rand()) / (float) RAND_MAX;
    //float c=2,s=3;

    //sw
        for (int i=0;i<N;i++){
        	sw_X[i]=X[i];
        	sw_Y[i]=Y[i];
        }

    OOPS_rot( N, X, incX, Y, incY, c, s);

    float tempx,tempy;

    for (int i=0;i<N;i++){
    	tempx=sw_X[i];
    	tempy=sw_Y[i];
        sw_X[i] =c*tempx+s*tempy;
        sw_Y[i]=c*tempy-s*tempx;
    }

    int match = 0;
    for(int i=0;i<N;i++){
        if(abs((sw_X[i]-X[i]))>0.01){
        	cout<< " SW X " << sw_X[i] << " HW_X " << X[i] <<endl;
            match = 1;
            break;
        }
        if(abs((sw_Y[i]-Y[i]))>0.01){
        	cout<< " SW_Y " << sw_Y[i] << " HW_Y " << Y[i] <<endl;
            match = 1;
            break;
        }

    }
    std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;
}

void main_rotm(){
    float* X;
    float* Y;
    float* P;
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    Y= (float *)OOPS_malloc(sizeof(float)*N*incY);
	P= (float *)OOPS_malloc(sizeof(float)*5);
	P[0]=1;
	P[1]=1;
	P[2]=2;
	P[3]=3;
	P[4]=4;
    vector_N(X,N,incX);
    vector_N(Y,N,incY);

//sw
    float* sw_X = (float *)OOPS_malloc(sizeof(float)*N*incX);
    float* sw_Y = (float *)OOPS_malloc(sizeof(float)*N*incY);
    for (int i=0;i<N;i++){
    	sw_X[i]=X[i];
    	sw_Y[i]=Y[i];
    }

float temp_x,temp_y;
if (P[0]==-1){
    for (int i=0;i<N;i++){
    	temp_x=sw_X[i];
    	temp_y=sw_Y[i];
        sw_X[i] =P[1]*temp_x+P[2]*temp_y;
        sw_Y[i]=P[3]*temp_x+P[4]*temp_y;
    }
}
else if (P[0]==0){
    for (int i=0;i<N;i++){
    	temp_x=sw_X[i];
    	temp_y=sw_Y[i];
        sw_X[i] =temp_x+P[2]*temp_y;
        sw_Y[i]=P[3]*temp_x+temp_y;
    }
}
else if (P[0]==1){
    for (int i=0;i<N;i++){
    	temp_x=sw_X[i];
    	temp_y=sw_Y[i];
        sw_X[i] =P[1]*temp_x+temp_y;
        sw_Y[i]=-temp_x+P[4]*temp_y;
    }
}
else if(P[0]==-2){
	for (int i=0;i<N;i++){
	    	temp_x=sw_X[i];
	    	temp_y=sw_Y[i];
	    	sw_X[i] = temp_x;
	        sw_Y[i] = temp_y;
	}
}

OOPS_rotm( N, X, incX, Y, incY, P);


    int match = 0;
    for(int i=0;i<N;i++){
        if(abs((sw_X[i]-X[i])/sw_X[i])>0.01){
        	std::cout << "i = " << i << " CPU result = " << sw_X[i] << " Device result = " << X[i] << std::endl;
            match = 1;
            break;
        }
        if(abs((sw_Y[i]-Y[i])/sw_Y[i])>0.01){
        	std::cout << "i = " << i << " CPU result = " << sw_Y[i] << " Device result = " << Y[i] << std::endl;
            match = 1;
            break;
        }
    }
    std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;


}

void main_scal(){
    float* X;
    float* X_sw;
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    X_sw= (float *)OOPS_malloc(sizeof(float)*N*incX);
    vector_N(X,N,incX);
    float alpha=((float) rand()) / (float) RAND_MAX;

    high_resolution_clock::time_point t1 = high_resolution_clock::now();

	for(int i=0;i<N;i++){
		X_sw[i] = alpha*X[i];
	}

    high_resolution_clock::time_point t2 = high_resolution_clock::now();

    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

    std::cout << "It took me " << time_span.count() << " seconds.";
    std::cout << std::endl;

	OOPS_scal( N, alpha, X, incX);

	int match = 0;
   	for (int i = 0; i < N; i++) {
   		if (abs(X_sw[i]-X[i])>0.001) {
   			std::cout << "Error: Result mismatch" << std::endl;
   	        std::cout << "i = " << i << " CPU result = " << X_sw[i]
   	                      << " Device result = " << X[i] << std::endl;
   	        match = 1;
   	        break;
   	    }
   	}

   	std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;

   	free(X);
   	free(X_sw);

}

void main_swap(){
    float* X;
    float* Y;
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    Y= (float *)OOPS_malloc(sizeof(float)*N*incY);
    vector_N(X,N,incX);
    vector_N(Y,N,incY);
    OOPS_swap( N, X, incX, Y, incY);
}

void main_axpy(){
    float* X;
    float* Y;
    float* Y_sw;
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    Y= (float *)OOPS_malloc(sizeof(float)*N*incY);
    Y_sw= (float *)OOPS_malloc(sizeof(float)*N*incY);

    vector_N(X,N,incX);
    vector_N(Y,N,incY);
    float alpha=((float) rand()) / (float) RAND_MAX;

    for(int i=0;i<N;i++){
      	Y_sw[i]=Y[i];
    }

    for(int i=0;i<N;i++){
       	Y_sw[i] = Y_sw[i]+alpha*X[i];
     }


    OOPS_axpy( N, alpha, X, incX, Y, incY);

    int match = 0;
    for (int i = 0; i < N; i++) {
    	if (abs(Y_sw[i]-Y[i])>0.001) {
    		std::cout << "Error: Result mismatch" << std::endl;
           	std::cout << "i = " << i << " CPU result = " << Y_sw[i]<< " Device result = " << Y[i] << std::endl;
           	match = 1;
           	break;
        }
     }

     std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;

     free(X);
     free(Y);
     free(Y_sw);

}

void main_xpay(){
	float* X;
    float* Y ,*Y_sw;
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    Y= (float *)OOPS_malloc(sizeof(float)*N*incY);
    Y_sw= (float *)OOPS_malloc(sizeof(float)*N*incY);
    vector_N(X,N,incX);
    vector_N(Y,N,incY);
    float alpha=((float) rand()) / (float) RAND_MAX;

    for(int i=0;i<N;i++){
    	Y_sw[i]=Y[i];
    }


    for(int i=0;i<N;i++){
    	Y_sw[i] = alpha*Y_sw[i]+X[i];
   	}

    OOPS_xpay( N, alpha, X, incX, Y, incY);

    int match = 0;
    for (int i = 0; i < N; i++) {
       if (abs(Y_sw[i]-Y[i])>0.001) {
    	   std::cout << "Error: Result mismatch" << std::endl;
       	   std::cout << "i = " << i << " CPU result = " << Y_sw[i]<< " Device result = " << Y[i] << std::endl;
       	   match = 1;
       	   break;
       }
    }

    std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;

    free(X);
    free(Y);
    free(Y_sw);
}

void main_asum(){
	float result,sw_result=0;
    float* X;
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    vector_N(X,N,incX);

    for(int i=0;i<N;i++){
    	sw_result +=abs(X[i]);
    }

    result=OOPS_asum( N, X, incX);

    if(abs(sw_result-result)>0.01){
    	std::cout << "Error: Result mismatch" << std::endl;
    	std::cout << "SW " << sw_result << " HW " << result << std::endl;

    }
    else{
        std::cout << "TEST PASSED" << std::endl;
    }

    free(X);
}

void main_iamax(){
	int result,sw_result=0;
	float temp=0,atemp;
    float* X;
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    vector_N(X,N,incX);
    result=OOPS_iamax( N, X, incX);

    for(int i=0;i<N;i++){
    	atemp=abs(X[i]);
    	if(atemp>temp){
    		temp=atemp;
    		sw_result=i;
    	}
    }


    if(abs(sw_result-result)>0.01){
       std::cout << "Error: Result mismatch" << std::endl;
       std::cout << "SW " << sw_result << " HW " << result << std::endl;

   }
   else{
	   std::cout << "TEST PASSED" << std::endl;
   }
    free(X);
}

void main_gemv(){

	int cols=1024,rows=1024;
    int lda = M;
    //enum layout {ColMajor=N, RowMajor=M};
    //layout layout_item;
    char TransA = 'N'; // ='C', = 'T' :options
    float* X = (float *)OOPS_malloc(sizeof(float)*cols*incX);
    float* Y = (float *)OOPS_malloc(sizeof(float)*rows*incY);
    float* Y_sw = (float *)OOPS_malloc(sizeof(float)*rows*incY);
    float* A = (float *)OOPS_malloc(sizeof(float)*cols*rows);

    vector_N(X,cols,incX);
    vector_N(Y,rows,incY);
    MxN_matrix(A,rows,cols);
    float alpha=((float) rand()) / (float) RAND_MAX;
    float beta=((float) rand()) / (float) RAND_MAX;

    //verify

    for(int i=0;i<rows;i++){
    	Y_sw[i]=Y[i];
    }

    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    for (int i = 0; i < rows; i++) {
		Y_sw[i]=beta*Y_sw[i];
		for (int j = 0; j < cols; j++){
			Y_sw[i]+=A[i*cols+j]*alpha*X[j];
		}
	}

    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

    std::cout << "It took me " << time_span.count() << " seconds.";
    std::cout << std::endl;

    OOPS_gemv(TransA,rows, cols,alpha,A,lda, X, incX,beta, Y, incY);

   	  //verify
   	    int match = 0;
   	    for (int i = 0; i < rows; i++) {
   	        if (abs(Y_sw[i]- Y[i])>0.001) {
   	            std::cout << "Error: Result mismatch" << std::endl;
   	            std::cout << "i = " << i << " CPU result = " << Y_sw[i]
   	                      << " Device result = " << Y[i] << std::endl;
   	            match = 1;
   	           // break;
   	        }
   	    }

   	    std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;

    free(X);
    free(Y);
    free(Y_sw);
    free(A);
}


void main_trmv(){
    float* X;
    float *A;
    int lda = N;
    //enum layout {ColMajor=N, RowMajor=M};
    //layout layout_item;
    char Uplo = 'U'; // ='U',='u', or ='L','l' for lower
    char TransA = 'N'; // ='C', = 'T' :options
    char Diag='N'; // ='n', or ='U','u', for unit triangular matrix
    X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    A= (float *)OOPS_malloc(sizeof(float)*N*N);
    vector_N(X,N,incX);
    triangular_MxN_matrix( Uplo,A, N);
/*
std::cout<<"Sw time"<<std::endl;
start_host = std::chrono::system_clock::now();
    //verify
    float sw_results[N];
    for (int i=0;i<N;i++){
    	sw_results[i]=0;
    }
    int x_index=0;
     if (Uplo == 'U' or Uplo == 'u'){
        for (int i = 0; i < N; i++) {
             x_index=i*incX;
            for (int j = i ;j < N; j++){
               sw_results[i]=sw_results[i]+A[i*N+j]*X[x_index];
				x_index=x_index+incX;
            }
        }
    }
    else if (Uplo == 'L' or Uplo == 'l'){
        for (int i = 0; i < N; i++) {
             x_index=0;
            for (int j = 0 ;j < i+1; j++){
                sw_results[i]=sw_results[i]+A[i*N+j]*X[x_index];
				x_index=x_index+incX;
            }
        }
    }
    else {
        std::cout<<"Define Uplo"<<std::endl;
    }
        	    ending_host = std::chrono::system_clock::now();
	 	      	    std::chrono::duration<double> elapsed_seconds = ending_host - start_host;
	 	      	        std::time_t end_time = std::chrono::system_clock::to_time_t(ending_host);
	 	      	        std::cout << "finished computation at " << std::ctime(&end_time)
	 	      	                  << "elapsed time: " << elapsed_seconds.count()/1000 << "ms\n";
    //call kernel
	 	      	        */
    OOPS_trmv(Uplo,TransA,Diag, N,A,lda, X, incX);

    //verify
    /*
    x_index=0;
   	    int match = 0;
   	    for (int i = 0; i < N; i++) {
   	        if (abs(X[x_index]- sw_results[i])/sw_results[i]>0.001) {
   	            std::cout << "Error: Result mismatch" << std::endl;
   	            std::cout << "i = " << i << " CPU result = " << sw_results[i]
   	                      << " Device result = " << X[x_index] << std::endl;
   	            match = 1;
   	            break;
   	        }
   	        x_index=x_index + incX;
   	    }
   	    std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;
*/
    free(X);
    free(A);
}

void main_trsv(){
    const int N=2048;
    int incX=1;
    int lda = N;
    //enum layout {ColMajor=N, RowMajor=M};
    //layout layout_item;
    char Uplo = 'L'; // ='U',='u', or ='L','l' for lower
    char TransA = 'N'; // ='C', = 'T' :options
    char Diag='N'; // ='n', or ='U','u', for unit triangular matrix
    float* X= (float *)OOPS_malloc(sizeof(float)*N*incX);
    float* A= (float *)OOPS_malloc(sizeof(float)*N*N);

    float* sw_results= (float *)OOPS_malloc(sizeof(float)*N*incX);

    vector_N(X,N,incX);
    memcpy(sw_results, X, sizeof(float)*N);

    triangular_NxN_matrix(Uplo,A,N);

    //verify
    if (Uplo == 'U' or Uplo == 'u'){
        for (int i = 0; i < N; i++) {
            //sw_results[i]=0;
            for (int j = i ;j < N; j++){
                //sw_results[i]+=X[j]*A[i*N+j];
            }
        }
    }
    else if (Uplo == 'L' or Uplo == 'l'){
        for (int i = 0; i < N; i++) {
                float a = sw_results[i];
                for (int j = 0; j < i; j++){
                    a-=sw_results[j]*A[i*N+j];
                }
                sw_results[i] = a;
            }
    }
    else {
        std::cout<<"Define Uplo"<<std::endl;
    }

    OOPS_trsv(Uplo, TransA, Diag, N, A, lda, X, incX);

    //verify
    int match = 0;
    for (int i = 0; i < N; i++) {
        std::cout << "i = " << i << " CPU result = " << sw_results[i]<< " Device result = " << X[i];
        if (abs(X[i]- sw_results[i])/sw_results[i]>0.001) {
            std::cout << "  Error: Result mismatch";
            match = 1;
        }
        std::cout << std::endl;
    }
    
    std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;

    free(X);
    free(A);
    free(sw_results);
}

void main_gemm(){

	const int K=2048,Mnew=2048,Nnew=2048;
    int lda = Mnew;
    int ldb = K;
    int ldc = Mnew;
    char TransA = 'N'; // ='C', = 'T' :options
    char TransB = 'N'; // ='C', = 'T' :options

    float* A= (float *)OOPS_malloc(sizeof(float)*K*Mnew);
    float* B= (float *)OOPS_malloc(sizeof(float)*K*Nnew);
    float* C= (float *)OOPS_malloc(sizeof(float)*Nnew*Mnew);

    if(TransA=='T' && TransB=='T'){
        MxN_matrix(A,K,Mnew);
        MxN_matrix(B,Nnew,K);
    }
    else if(TransA=='N' && TransB=='T'){
        MxN_matrix(A,Mnew,K);
        MxN_matrix(B,Nnew,K);
    }
    else if(TransA=='T' && TransB=='N'){
        MxN_matrix(A,K,Mnew);
        MxN_matrix(B,K,Nnew);
    }
    else{
        MxN_matrix(A,Mnew,K);
        MxN_matrix(B,K,Nnew);
    }

    MxN_matrix(C,Mnew,Nnew);

    float alpha=((float) rand()) / (float) RAND_MAX;
    float beta=((float) rand()) / (float) RAND_MAX;

    //verify
	float* C_sw= (float *)OOPS_malloc(sizeof(float)*Nnew*Mnew);


    high_resolution_clock::time_point t1 = high_resolution_clock::now();


	for (int i = 0; i < Mnew; i++) {
		for (int j = 0; j < Nnew; j++){
			C_sw[i*Nnew+j]=beta*C[i*Nnew+j];
			for (int k=0; k<K;k++){
				if(TransA=='T' && TransB=='T'){
					C_sw[i*Nnew+j]+=alpha*A[k*Mnew+i]*B[j*K+k];
				}
				else if(TransA=='N' && TransB=='T'){
					C_sw[i*Nnew+j]+=alpha*A[i*K+k]*B[j*K+k];
				}
				else if(TransA=='T' && TransB=='N'){
					C_sw[i*Nnew+j]+=alpha*A[k*Mnew+i]*B[k*Nnew+j];
				}
				else{
					C_sw[i*Nnew+j]+=alpha*A[i*K+k]*B[k*Nnew+j];
				}
			}
		}
	}

	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

	std::cout << "It took me " << time_span.count() << " seconds.";
	std::cout << std::endl;

    OOPS_gemm(TransA, TransB, Mnew, Nnew, K, alpha, A, lda, B, ldb, beta, C, ldc);

   	//verify
   	int match = 0;
   	for (int i = 0; i < Mnew*Nnew; i++) {
   		if (abs(C_sw[i]-C[i])>0.001) {
   			std::cout << "Error: Result mismatch" << std::endl;
   	        std::cout << "i = " << i << " CPU result = " << C_sw[i]
   	                      << " Device result = " << C[i] << std::endl;
   	        match = 1;
   	        break;
   	    }
   	}

   	std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;

    free(A);
    free(B);
    free(C);
    free(C_sw);
}

void main_trmm(){

	const int Mnew=2048,Nnew=2048;
    int lda = Mnew;
    int ldb = Mnew;
    char TransA = 'N'; // ='C', = 'T' :options

    float* A= (float *)OOPS_malloc(sizeof(float)*Mnew*Mnew);
    float* B= (float *)OOPS_malloc(sizeof(float)*Mnew*Nnew);
    float* C= (float *)OOPS_malloc(sizeof(float)*Mnew*Nnew);



    MxN_matrix(B,Mnew,Nnew);
    triangular_MxN_matrix('U',A,Nnew);


    float alpha=((float) rand()) / (float) RAND_MAX;

    //verify
	float* B_sw= (float *)OOPS_malloc(sizeof(float)*Nnew*Mnew);

    high_resolution_clock::time_point t1 = high_resolution_clock::now();


	for (int i = 0; i < Mnew; i++) {
		for (int j = i; j < Nnew; j++){
			B_sw[i*Nnew+j]=0;
			for (int k=i; k<Mnew;k++){
				B_sw[i*Nnew+j]=alpha*A[i*Mnew+k]*B[k*Nnew+j];
			}
		}
	}

	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

	std::cout << "It took me " << time_span.count() << " seconds.";
	std::cout << std::endl;

    OOPS_trmm('L','U',TransA, 'N', Mnew, Nnew, alpha, A, lda, B, ldb, C);

   	//verify
   	int match = 0;
   	for (int i = 0; i < Mnew; i++) {
   		for(int j=i;j<Nnew;j++){
   			if (abs(B_sw[i*Nnew+j]-C[i*Nnew+j])>0.001) {
   				std::cout << "Error: Result mismatch" << std::endl;
   			   	std::cout << "i = " << i*Nnew+j << " CPU result = " << B_sw[i*Nnew+j]
   			   	                      << " Device result = " << C[i*Nnew+j] << std::endl;
   			   	match = 1;
   			   	//break;
   			}
   		}
   	}

   	std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;

    free(A);
    free(B);
    free(B_sw);
}

void main_symm(){

	const int Mnew=2048,Nnew=2048;
    int lda = Mnew;
    int ldb = Mnew;
    int ldc = Mnew;

    float* A= (float *)OOPS_malloc(sizeof(float)*Mnew*Mnew);
    float* B= (float *)OOPS_malloc(sizeof(float)*Mnew*Nnew);
    float* C= (float *)OOPS_malloc(sizeof(float)*Mnew*Nnew);



    MxN_matrix(B,Mnew,Nnew);
    triangular_MxN_matrix('U',A,Nnew);
    MxN_matrix(C,Mnew,Nnew);


    float alpha=((float) rand()) / (float) RAND_MAX;
    float beta=((float) rand()) / (float) RAND_MAX;

    //verify
	float* C_sw= (float *)OOPS_malloc(sizeof(float)*Nnew*Mnew);

	for (int i = 0; i < Mnew; i++) {
		for (int j = 0; j < Nnew; j++){
			C_sw[i*Nnew+j]=C[i*Nnew+j];

		}
	}

    high_resolution_clock::time_point t1 = high_resolution_clock::now();


	for (int i = 0; i < Mnew; i++) {
		for (int j = 0; j < Nnew; j++){
			C_sw[i*Nnew+j]=beta*C_sw[i*Nnew+j];
			for (int k=0; k<Mnew;k++){
				if(i<=k){
					C_sw[i*Nnew+j]+=alpha*A[i*Mnew+k]*B[k*Nnew+j];
				}
				else{
					C_sw[i*Nnew+j]+=alpha*A[k*Mnew+i]*B[k*Nnew+j];
				}
			}
		}
	}

	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

	std::cout << "It took me " << time_span.count() << " seconds.";
	std::cout << std::endl;

    OOPS_symm('L','U', Mnew, Nnew, alpha, A, lda, B, ldb, beta, C, ldc);

   	//verify
   	int match = 0;
   	for (int i = 0; i < Mnew; i++) {
   		for(int j=0;j<Nnew;j++){
   			if (abs(C_sw[i*Nnew+j]-C[i*Nnew+j])>0.001) {
   				std::cout << "Error: Result mismatch" << std::endl;
   			   	std::cout << "i = " << i*Nnew+j << " CPU result = " << C_sw[i*Nnew+j]
   			   	                      << " Device result = " << C[i*Nnew+j] << std::endl;
   			   	match = 1;
   			   	//break;
   			}
   		}
   	}

   	std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;

    free(A);
    free(B);
    free(C);
    free(C_sw);
}

void main_trsm(){

	const int Mnew=2048,Nnew=2048;
    int lda = Mnew;
    int ldb = Mnew;
    char TransA = 'N'; // ='C', = 'T' :options

    float* A= (float *)OOPS_malloc(sizeof(float)*Mnew*Mnew);
    float* B= (float *)OOPS_malloc(sizeof(float)*Mnew*Nnew);
    float* C= (float *)OOPS_malloc(sizeof(float)*Mnew*Nnew);



    MxN_matrix(B,Mnew,Nnew);
    triangular_MxN_matrix('U',A,Nnew);


    float alpha=((float) rand()) / (float) RAND_MAX;

    //verify
	float* B_sw= (float *)OOPS_malloc(sizeof(float)*Nnew*Mnew);

    high_resolution_clock::time_point t1 = high_resolution_clock::now();


	for (int i = 0; i < Mnew; i++) {
		for (int j = i; j < Nnew; j++){
			B_sw[i*Nnew+j]=0;
			for (int k=i; k<Mnew;k++){
				B_sw[i*Nnew+j]+=(alpha*B[k*Nnew+j])/A[i*Mnew+k];
			}
		}
	}

	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

	std::cout << "It took me " << time_span.count() << " seconds.";
	std::cout << std::endl;

    OOPS_trsm('L','U',TransA, 'N', Mnew, Nnew, alpha, A, lda, B, ldb, C);

   	//verify
   	int match = 0;
   	for (int i = 0; i < Mnew; i++) {
   		for(int j=i;j<Nnew;j++){
   			if (abs(B_sw[i*Nnew+j]-C[i*Nnew+j])>0.001) {
   				std::cout << "Error: Result mismatch" << std::endl;
   			   	std::cout << "i = " << i*Nnew+j << " CPU result = " << B_sw[i*Nnew+j]
   			   	                      << " Device result = " << C[i*Nnew+j] << std::endl;
   			   	match = 1;
   			   	//break;
   			}
   		}
   	}

   	std::cout << "TEST " << (match ? "FAILED" : "PASSED") << std::endl;

    free(A);
    free(B);
    free(C);
    free(B_sw);
}

void main_SpMV(){
    int nrows = 0;
    int nterm = 0;
    int * irow = NULL;
    int * iat = NULL;
    int * ja = NULL;
    float * coef = NULL;

    ifstream file_A_ascii("m3e_matrix.coo");
    file_A_ascii >> nrows >> nterm;
    printf("\tnrows = %d\n",nrows);
    printf("\tnterm = %d\n",nterm);

    // Allocate space
    irow = (int*) OOPS_malloc(nterm * sizeof(int));
    iat  = (int*) OOPS_malloc((nrows+1) * sizeof(int));
    ja  = (int*) OOPS_malloc(nterm * sizeof(int));
    coef = (float*) OOPS_malloc(nterm * sizeof(float));

    // Read entries
    for ( int i = 0; i < nterm; i++) {
        file_A_ascii >> irow[i] >> ja[i] >> coef[i];
        irow[i]--;
        ja[i]--;
    }
    // for(int i=0; i<100; i++)
    //    std::cout << irow[i] << "\t" << ja[i] << "\t" << coef[i] << "\n";

    // Assembly iat for the Compressed Sparse Row (CSR) format
    // Note that the matrix is symmetric so CSR format == CSC format
    iat[0] = 0;
    int j = 0;
    for ( int i = 0; i < nterm; i++) {
        if( irow[i] > j ) {
            iat[j+1] = i;
            j++;
        }
    }
    iat[nrows] = nterm;

    // Free scratch
    free(irow);

    // Close file
    file_A_ascii.close();

    float * x = (float*) OOPS_malloc(nrows * sizeof(float));
    for (int i=0; i<nrows; i++)
        x[i] = 1.0;

    float * b = (float*) OOPS_malloc(nrows * sizeof(float));
    for ( int i = 0; i < nrows; i++)
        b[i] = 0;

    OOPS_SpMV();
    OOPS_SpMV(nrows, nterm, iat, ja, coef, x, b);

    //verify
    /* ok verified! */

    free(x);
    free(y);
    free(iat);
    free(ja);
    free(coef);
}


int main(int argc, char** argv) {
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << " <XCLBIN File>" << std::endl;
        return EXIT_FAILURE;
    }

    program_device(argv[1]);

    main_trmv();


    release_device();

    return EXIT_SUCCESS;

}
