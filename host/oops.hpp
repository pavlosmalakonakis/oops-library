#include "xcl2.hpp"
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <unistd.h>
#include "ap_int.h"

using namespace std;

typedef ap_uint<32>  ColType;
typedef ap_uint<1> RowType;

//N (int)~> number of elements in input vector
//X (float) ~> a float array (input)
//Y (float) ~> a float array (output)
//incX (int) ~> storage spacing between elements of X
//incY (int) ~> storage spacing between elements of Y
void OOPS_copy( const int N, const float *X, const int incX, float *Y, const int incY);

//N (int)~> number of elements in input vector
//X (float) ~> a float array (input)
//Y (float) ~> a float array (input)
//incX (int) ~> storage spacing between elements of X
//incY (int) ~> storage spacing between elements of Y
float OOPS_dot( const int N, const float *X, const int incX, const float *Y, const int incY);

//N (int)~> number of elements in input vector
//X (float) ~> a float array (input)
//Y (float) ~> a float array (input)
//alpha (float)~> specifies the scalar alpha
//incX (int) ~> storage spacing between elements of X,
//incY(int) ~> storage spacing between elements of Y
float OOPS_ddot( const int N, const float alpha, const float *X, const int incX, const float *Y, const int incY);

//N (int)~> number of elements in input vector
//X (float) ~> a float array (input/output)
//Y (float) ~> a float array (input/output)
//incX (int) ~> storage spacing between elements of X
//incY (int) ~> storage spacing between elements of Y
// c (float) ~> specifies the scalar c
//s (float) ~> specifies the scalar s
void OOPS_rot( const int N, float *X, const int incX, float *Y, const int incY, const float  c, const float  s);

//N (int)~> number of elements in input vector
//X (float) ~> a float array (output)
//Y (float) ~> a float array (output)
//incX (int) ~> storage spacing between elements of X
//incY (int) ~> storage spacing between elements of Y
// P (float) ~> a float parameter array with dimension 5, param(1) contains a switch, flag. param(2-5) contain h11, h21, h12, and h22, respectively
void OOPS_rotm( const int N, float *X, const int incX, float *Y, const int incY, const float *P);

//N (int)~> number of elements in input vector
//X (float) ~> is a float array (input/output)
//alpha (float)~> specifies the scalar alpha
//incX (int) ~> storage spacing between elements of X
void OOPS_scal( const int N, const float alpha, float *X, const int incX);

//N (int)~> number of elements in input vector
//X (float) ~> a float array (input/output)
//Y (float) ~> a float array (input/output)
//incX (int) ~> storage spacing between elements of X
//incY (int) ~> storage spacing between elements of Y
void OOPS_swap( const int N, float *X, const int incX, float *Y, const int incY);

//N (int)~> number of elements in input vector
//X (float) ~> a float array (input)
//Y (float) ~> a float array (input/output)
//alpha (float)~> specifies the scalar alpha
//incX (int) ~> storage spacing between elements of X
//incY(int) ~> storage spacing between elements of Y
void OOPS_axpy(const int N, const float alpha, const float *X, const int incX, float *Y, const int incY);

//N (int)~> number of elements in input vector
//X (float) ~> a float array (input)
//Y (float) ~> a float array (input/output)
//alpha (float)~> specifies the scalar alpha
//incX (int) ~> storage spacing between elements of X
//incY(int) ~> storage spacing between elements of Y
void OOPS_xpay(const int N, const float alpha, const float *X, const int incX, float *Y, const int incY);

//N (int)~> number of elements in input vector
//X (float) ~> is a float array
//incX (integer) ~> storage spacing between elements of X
float OOPS_asum(const int N, const float *X, const int incX);

//N (int)~> number of elements in input vector
//X (float) ~> is a float array
//incX (int) ~> storage spacing between elements of X
int OOPS_iamax(const int N, const float  *X, const int incX);

//N (int)~> number of elements in input vector
//X (float) ~> is a float array
//incX (int) ~> storage spacing between elements of X
//returns the euclidean norm of a vector via the function
float OOPS_nrm2( const int N, const float *X, const int incX);

//Trans (char) ~> specifies the operation to be performed
//M (int) ~> specifies the number of rows of matrix A
//N (int) ~> specifies the number of columns of matrix A
//alpha (float) ~> specifies the scalar alpha
//beta (float) ~> specifies the scalar beta, when beta is zero then Y need not be set on input
//A (float) ~> a float array of dimension LDA, N
//lda (int) ~> specifies the first dimension of A
//X (float) ~> a float array
//Y (float) ~> a float array
//incX (int) ~> storage spacing between elements of X
//incY (int) ~> storage spacing between elements of Y
//performs one of the matrix-vector operations
//y := alpha*A*x + beta*y,   or   y := alpha*A**T*x + beta*y,
//where alpha and beta are scalars, x and y are vectors and A is an
//m by n matrix.
void OOPS_gemv(const char TransA, const int M, const int N, const float alpha, const float  *A, const int lda,
                 const float  *X, const int incX, const float beta, float  *Y, const int incY);

//Uplo (char) ~> specifies whether the upper or lower triangular part of array A is to be referenced
//N (int) ~> specifies the order of matrix A
//alpha (float) ~> specifies the scalar alpha
//beta (float) ~> specifies the scalar beta, when beta is zero then Y need not be set on input
//A (float) ~> a float array of dimension LDA, N
//lda (int) ~> specifies the first dimension of A
//X (float) ~> a float array
//Y (float) ~> a float array
//incX (int) ~> storage spacing between elements of X
//incY (int) ~> storage spacing between elements of Y
//performs the matrix-vector  operation
//y := alpha*A*x + beta*y,
//where alpha and beta are scalars, x and y are n element vectors and
//A is an n by n symmetric matrix.
void OOPS_symv(const char Uplo, const int N, const float alpha, const float  *A, const int lda,
                 const float  *X, const int incX, const float beta, float  *Y, const int incY);

//layout ~> enum {ColMajor, RowMajor}
//Uplo (char) ~> specifies whether the matrix A is an upper or lower triangular matrix
//Trans (char) ~> specifies the equation to be solved
//Diag (char) ~> specifies whether or not A is unit triangular
//N (int) ~> specifies the order of matrix Ap
//A (float) ~> a float array of dimension LDA, N
//lda (int) ~> specifies the first dimension of A
//X (float) ~> a float array
//incX (int) ~> storage spacing between elements of X
//performs one of the matrix-vector operations
//x := A*x,   or   x := A**T*x,
//where x is an n element vector and  A is an n by n unit, or non-unit,
//upper or lower triangular matrix.
void OOPS_trmv(const char Uplo, const char TransA, const char Diag,
                 const int N, const float  *A, const int lda, float  *X, const int incX);


//Trans (char) ~> specifies the operation to be performed
//M (int) ~> specifies the number of rows of matrix A
//N (int) ~> specifies the number of columns of matrix A
//KL (int) ~> specifies the number of sub-diagonals of matrix A
//KU (int) ~> specifies the number of super-diagonals of matrix A
//alpha (float) ~> specifies the scalar alpha
//beta (float) ~> specifies the scalar beta, when beta is zero then Y need not be set on input
//A (float) ~> a float array of dimension LDA, N
//lda (int) ~> specifies the first dimension of A
//X (float) ~> a float array , Y (float) ~> a float array
//incX (int) ~> storage spacing between elements of X
//incY (int) ~> storage spacing between elements of Y
//performs one of the matrix-vector operations
//y := alpha*A*x + beta*y,   or   y := alpha*A**T*x + beta*y,
//where alpha and beta are scalars, x and y are vectors and A is an
//m by n band matrix, with kl sub-diagonals and ku super-diagonals.
void OOPS_gbmv(const char TransA, const int M, const int N, const int KL, const int KU, const float alpha, const float *A, const int lda,
                 const float  *X, const int incX, const float beta, float  *Y, const int incY);

//Uplo (char) ~> specifies whether the matrix A is an upper or lower triangular matrix
//Trans (char) ~> specifies the equation to be solved
//Diag (char) ~> specifies whether or not A is unit triangular
//N (int) ~> specifies the order of matrix Ap
//A (float) ~> a float array of dimension LDA, N
//lda (int) ~> specifies the first dimension of A
//X (float) ~> a float array
//incX (int) ~> storage spacing between elements of X
//performs one of the matrix-vector operations
//A*x = b, or A'*x = b, or conjg(A')*x = b,
//where b and x are n-element vectors and A is an n-by-n unit, or non-unit, upper or lower triangular matrix.
//Before entry, the incremented array x must contain the n-element right-hand side vector b.
//upper or lower triangular matrix.
void OOPS_trsv(const char Uplo, const char TransA, const char Diag,
                 const int N, const float  *A, const int lda, float  *X, const int incX);

//TransA (char) ~> specifies the form of operation A to be used in the matrix multiplication
//TransB (char) ~> specifies the form of operation B to be used in the matrix multiplication
//M (int) ~> specifies  the number  of rows  of the  matrix operation A and of the matrix C
//N (int) ~> specifies  the number  of columns  of the  matrix operation B and of the matrix C
//K (int) ~> specifies  the number of columns of the matrix operation A and the number of rows of the matrix operation B
//alpha (float) ~> specifies the scalar alpha , beta (float) ~> specifies the scalar beta, when beta is zero then C need not be set on input
//A (float) ~> a float array of dimension LDA, K or M
//lda (int) ~> specifies the first dimension of A
//B (float) ~> a float array of dimension LDB, N or K
//ldB (int) ~> specifies the first dimension of B
//C (float) ~> a float array of dimension LDC, N
//ldC (int) ~> specifies the first dimension of C
//performs one of the matrix-matrix operations  C := alpha*op( A )*op( B ) + beta*C,
//where  op( X ) is one of
//op( X ) = X   or   op( X ) = X**T,
//alpha and beta are scalars, and A, B and C are matrices, with op( A )
//an m by k matrix,  op( B )  a  k by n matrix and  C an m by n matrix.
void OOPS_gemm(const char TransA, const char TransB, const int M, const int N, const int K, const float alpha, const float  *A, const int lda,
				const float  *B, const int ldb, const float beta, float  *C, const int ldc);

//Side (char) ~> specifies whether the operation A multiplies B from the left or right
//Uplo (char) ~> specifies whether the matrix A is an upper or lower triangular
//TransA (char) ~> specifies the form of operation A to be used in the matrix multiplication
//Diag (char) ~> specifies whether or not A is unit triangular
//M (int) ~> specifies  the number  of rows of the  matrix  B
//N (int) ~> specifies  the number  of columns  of the  matrix  B
//alpha (float) ~> specifies the scalar alpha
//A (float) ~> a float array of dimension LDA, M or N
//lda (int) ~> specifies the first dimension of A
//B (float) ~> a float array of dimension LDB, N
//ldB (int) ~> specifies the first dimension of B
//performs one of the matrix-matrix operations
//B := alpha*op( A )*B,   or   B := alpha*B*op( A ),
//where  alpha  is a scalar,  B  is an m by n matrix,  A  is a unit, or
//non-unit,  upper or lower triangular matrix  and  op( A )  is one  of
//op( A ) = A   or   op( A ) = A**T.
void OOPS_trmm(const char Side, const char Uplo, const  char TransA, const char Diag, const int M, const int N, const float alpha,
				const float *A, const int lda, const float *B, const int ldb, float *C);

//Side (char) ~> specifies whether the symmetric matrix A appears on the left or right in the operation
//Uplo (char) ~> specifies whether the symmetric matrix A is an upper or lower triangular
//M (int) ~> specifies  the number  of rows of the  matrix  C
//N (int) ~> specifies  the number  of columns  of the  matrix  C
//alpha (float) ~> specifies the scalar alpha , beta (float) ~> specifies the scalar beta, when beta is zero then C need not be set on input
//A (float) ~> a float array of dimension LDA, M or N
//lda (int) ~> specifies the first dimension of A
//B (float) ~> a float array of dimension LDB, N
//ldB (int) ~> specifies the first dimension of B
//C (float) ~> a float array of dimension LDC, N
//ldC (int) ~> specifies the first dimension of C
//performs one of the matrix-matrix operations
//C := alpha*A*B + beta*C,
//or
//C := alpha*B*A + beta*C,
//where alpha and beta are scalars,  A is a symmetric matrix and  B and
//C are  m by n matrices.
void OOPS_symm(const char Side, const char Uplo, const int M, const int N, const float alpha, const float  *A, const int lda, const float  *B,
		const int ldb, const float beta, float  *C, const int ldc);

//Side (char) ~> specifies whether the operation A multiplies B from the left or right
//Uplo (char) ~> specifies whether the matrix A is an upper or lower triangular
//TransA (char) ~> specifies the form of operation A to be used in the matrix multiplication
//Diag (char) ~> specifies whether or not A is unit triangular
//M (int) ~> specifies  the number  of rows of the  matrix  B
//N (int) ~> specifies  the number  of columns  of the  matrix  B
//alpha (float) ~> specifies the scalar alpha
//A (float) ~> a float array of dimension LDA, M or N
//lda (int) ~> specifies the first dimension of A
//B (float) ~> a float array of dimension LDB, N
//ldB (int) ~> specifies the first dimension of B
//solves one of the matrix equations
//op( A )*X = alpha*B,   or   X*op( A ) = alpha*B,
//where alpha is a scalar, X and B are m by n matrices, A is a unit, or
//non-unit,  upper or lower triangular matrix  and  op( A )  is one  of
//op( A ) = A   or   op( A ) = A**T.
void OOPS_trsm(const char Side, const char Uplo, const  char TransA, const char Diag, const int M, const int N, const float alpha,
		const float *A, const int lda, const float *B, const int ldb, float *C);

// [in] nrows number of rows
// [in] nterm number of entries
// [in] iat pointers to the beginning of each row in ja and coef
// [in] ja column indices of the matrix entries
// [in] coef matrix coefficients
// [in] x array of real
// [out] b array of real
void OOPS_SpMV(const int nrows, const int nterm,
              const long int* iat, const int* ja, const double* __restrict__ coef,
              const double* __restrict__ x, double* __restrict__ b);

//function that programs device with xclbin file
void program_device(const char *arg1);

//OOPS memory allocation. Allocates aligned memory based on OS page size
void *OOPS_malloc(size_t alloc_bytes);


/*
    LU Decomposition Kernel Implementation
    	float * A         --> input Matrix A
    	float * L         --> Output Matrix L
    	float * U         --> Output Matrix U
    	int N			  --> size of matrix A
*/
void OOPS_lu(float *A, float *L, float *U, int N);

//function that release OpenCL objects and device
void release_device();

inline cl::Context context;
inline cl::Program program;
inline cl::CommandQueue q;
